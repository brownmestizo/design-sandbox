module.exports = function (grunt) {


    // Define the configuration for all the tasks
    grunt.initConfig({

        watch: {

            styles: {
                files: [
                    'assets/scss/**/*.scss', 
                    'mpm-central/assets/scss/**/*.scss',                     
                ],
                tasks: ['sass'],
                options: {
                    interval:1000,
                }
            },

            copy: {
                files: ['modelbuffs.com/*'],
                tasks: ['copy'],
            },            

        },

        sass: {

            dist: {
                files: {
                    'assets/css/style-mb.css': 'assets/scss/style-mb.scss',
                    'assets/css/style-pa.css': 'assets/scss/style-pa.scss',
                    'assets/css/style-m3.css': 'assets/scss/style-m3.scss',                    
                    'mpm-central/assets/css/style.css': 'mpm-central/assets/scss/style.scss',
                },            
                options: {
                        //debugInfo: true,
                        lineNumbers: true
                }
            }
        },

        copy: {
          planearts: {
            expand: true,
            cwd: 'modelbuffs.com/',
            src: ['**', 
                '!views/partials/banner-frontPage.php',
                '!views/page_index.html',
                '!views/partials/navbar.php',
            ],
            dest: 'planearts.com/',
          },
          mymahogany: {
            expand: true,
            cwd: 'modelbuffs.com/',
            src: ['**', 
                '!views/partials/banner-frontPage.php',
                '!views/page_index.html'
            ],
            dest: 'mymahoganymodel.com/',
          },          
        },        

    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default',['watch','sass']);

};
