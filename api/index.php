<?php

use MB\Cart\Cart;
use MB\Templating\Templater;

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once '../lib/init.php';
$websiteVars = getWebsiteVars($_SERVER['HTTP_REFERER']);

require '../vendor/slim/slim/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->post('/cart/payed/paypal', function () use ($websiteVars) {
    $r = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
    $info = $r->get('info');

    $id = null;
    if (!$info || !($info = json_decode($info, true)) || !$id = $info['id']) {
        die(json_encode(['error' => 'No id provided']));
    }

    //load cart
    $dc = new \MB\Cart\DbCartStorage();
    $cart = $dc->load();
    //and save it with other id to have an ability to use session
    $id = $dc->saveWithNewId($cart, array_merge((array)$info , ['provider' => 'paypal', 'id' => $id]));
    $dc->save(new Cart());

    //We don't want user to wait for message to be send (some seconds)
    ob_end_clean();
    header("Connection: close");
    ignore_user_abort(); // optional
    ob_start();
    echo json_encode(['ok' => $id]);
    $size = ob_get_length();
    header("Content-Length: $size");
    ob_end_flush();
    flush();
    session_write_close();

    $twig = Templater::getTwig();
    // Send message
    $mailer = new \Mail\Mailer(Swift_SmtpTransport::newInstance(
        $websiteVars['emailServer'],
        $websiteVars['emailSMTPSPort'],
        'ssl'
    )
        ->setUsername($websiteVars['emailAddress'])
        ->setPassword($websiteVars['emailPassword'])
    );
    $mailer->send(
        $websiteVars['emailAddress'],
        $info['payer']['payer_info']['email'],
        'Purchase details',
        $twig->render('customer.html', compact('info')),
        [$websiteVars['contactEmailAddress']]
    );
});

$app->get('/products/search/:search', function ($search) {

    $searchTerms = explode(' ', $search);

    $r = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
    $notids = PkList::fromString($r->get('not', ''))->pks();

    $prod = [];

    foreach($searchTerms as $x) {

        $prod = TblProdInfoQuery::create()
            ->select(['prod_id', 'prod_name'])->orderBy('prod_name')
            ->filterByProdName('%' . strtoupper($x) . '%', \Propel\Runtime\ActiveQuery\Criteria::LIKE)->limit(100)
            ->where('tbl_prod_info.prod_id NOT IN ?', $notids);

    }

//  echo $prod->toString();
    echo $prod->find()->toJSON();
});


$app->get('/products/photos/:id', function ($id) {
    $prod = TblProdPhotosv2Query::create()->findOneByProdId($id);
    echo $prod->toJSON();
});

$app->get('/product/:product', function ($product) {
    $productQuery = new TblProdInfoQuery();
    $product = $productQuery->findPK($product);
    $product->getTblProdPricing()->getProdPricePrice();
    $product->getTblProdPhotosv2();
    echo $product->toJSON();
});
$app->get('/stands/all', function () {
    $standQuery = new TblStandsQuery();
    $stands = $standQuery->orderByStandId()->find();
    echo $stands->toJSON();
});

$app->get('/fedex/country/:search', function ($search) {
    $countriesQuery = TblShippingCountriesQuery::create()
//        ->select(array('cty_id', 'cty_name'))
        ->filterByCtyName(strtoupper($search) . '%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);

    echo $countriesQuery->find()->toJSON();
});

$app->get('/fedex/country/options/:id', function ($id) {
    $cs = new \MB\Cart\DbCartStorage();
    $cart = $cs->load();
    $shipping = new \MB\Shipping\Shipping($id);
    $priority = $shipping->getPriorityPrice($cart);
    $air = $shipping->getAirParcelPrice($cart);
    $economy = $shipping->getEconomyPrice($cart);
    $products = $shipping->getProductPrices($cart);
    echo json_encode(compact('priority', 'economy', 'products', 'air'));
});

$app->get('/fedex/countries', function () {
    $countriesQuery = TblShippingCountriesQuery::create()
        ->select(['cty_id', 'cty_name'])
        ->find();

    echo $countriesQuery->toJSON();
});

$app->get('/price/:product', function ($product) {
    $product = TblProdInfoQuery::create()
        ->select(['prod_price_id'])
        ->filterByProdId($product)
        ->findOne();

    $pricingQuery = new TblProdPricingQuery();

    $price = $pricingQuery
        ->filterByProdPriceId($product)
        ->findOne();

    echo $price->toJSON();
});

$app->run();
