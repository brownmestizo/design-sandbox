$(document).ready(function() {
    paypal.Button.render({
        env: 'production', // Optional: specify 'sandbox' environment

        style: {
            size: 'medium',
            color: 'gold',
            shape: 'rect'
        },
        
        client: {
            sandbox:    'Aao0sA0H285TD3ZFt1aaMl_69-DjDw5aSNFHUVpkRgEmKvMFpwiW8vAtr8R7djUac5RrSXnfdnMPQKjd',
            production: 'AZgtNHgciGMoe8PMe1N7NmP9sSHk9Q9B9wGX3u-VHWtrb2a8qwePUd2wtuU-xXwS3DhLgboH2xJ18jDy'
        },

        payment: function() {
        
            var env    = this.props.env;
            var client = this.props.client;

            var transaction = {
                'amount' : {
                    'total' : totalPrice,
                    'currency' : 'USD',
                    'details' : {
                        'subtotal' : price,
                        'shipping' : shipping
                    }
                },
                "custom": id,
                "item_list": {'items': details},
                'description': 'Modelbuffs purchase'
            };
            return paypal.rest.payment.create(env, client, {
                transactions: [
                    transaction
                ]
            });
        },

        commit: true, // Optional: show a 'Pay Now' button in the checkout flow

        onAuthorize: function(data, actions) {
            return actions.payment.execute().then(function(info) {
                if (!info.state === 'approved') {
                    return;
                }
                $.post('../api/index.php/cart/payed/paypal', {info: JSON.stringify(info)}, function (data) {
                    if (!data.ok) {
                        return;
                    }
                    location.href = 'congratulation.php?id=' + data.ok;
                }, 'json');
            }, function () {
                alert('Your payment was unsuccessful, please try once more');
            });
        }

    }, '#paypal-button');
});
