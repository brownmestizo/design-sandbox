!(function () {
    // Initialize select2
    var select = $('.js-select-country'),
        priorityPrice = $('.js-priority-price'),
        economyPrice = $('.js-economy-price'),
        airPrice = $('.js-air-price'),
        airBlock = $('.js-air'),
        priorityBlock = $('.js-fedex-priority'),
        economyBlock = $('.js-fedex-economy'),
        totalShipping = $('.js-total-shipping'),
        totalSummary = $('.js-summary'),
        cartPrice = $('.js-cart-price').data('price')
        ;

    // Initialize select2
    select.select2({
        placeholder: 'Select country of destination'
    });

    // When select changes, we're trying to get shipping info for selected country
    select.change(function () {
        loadShippingOptionsFor(this.value);
    });

    // Shipping data will be stored here
    var xdata = {
            p: null, // priority shipping total cost
            e: null, // economy shipping total cost
            i: [],    // per product shipping cost
            a: null  // air shipping price
        },
        radios = $('.js-ship-radio'),
        eraseShippingInformation = function () {
            // uncheck radios
            radios.each(function () {
                this.checked = false;
                $(this).trigger('stateChanged');
            });

            economyPrice.add(priorityPrice).add(airPrice).html('-');
            economyBlock.add(priorityBlock).add(airBlock).hide();
            xdata = {p: null, e: null, i: [], a: null};
            changeSummary();
        },
        loadShippingOptionsFor = function (countryId) {
            eraseShippingInformation();
            $.get('../api/index.php/fedex/country/options/' + countryId, function (data) {
                if (data.priority) {
                    priorityPrice.html('$' + data.priority);
                    priorityBlock.show();
                    xdata.p = data.priority;
                }
                if (data.economy) {
                    economyPrice.html('$' + data.economy);
                    economyBlock.show();
                    xdata.e = data.economy;
                }
                if (data.air) {
                    airPrice.html('$' + data.air);
                    airBlock.show();
                    xdata.a = data.air;
                }
                xdata.i = data.products;
            }, 'json')
        },
        changeSummary = function () {
            var shippingType = $('.js-ship-radio:checked').val(),
                shippingPrice = 0;
            totalShipping.html('-');
            if (shippingType == 'priority') {
                shippingPrice = xdata.p;
            } else if (shippingType == 'economy') {
                shippingPrice = xdata.e;
            } else if (shippingType == 'air') {
                shippingPrice = xdata.a;
            }

            if (shippingPrice) {
                totalShipping.html('$' + shippingPrice);
            }

            totalSummary.html('$' + (Math.round((shippingPrice + cartPrice) * 100) / 100));

            changeProductsShippingDetails(shippingType);
        },
        changeProductsShippingDetails = function (shippingType) {
            var products = xdata.i,
                shippingPrice = 0;
            $('.js-prod-shipping').html('-');
            $('.js-prod-total').each(function () {
                this.innerHTML = '$' + this.getAttribute('data-price');
            });
            for (var i in products) {
                var product = products[i];
                if (shippingType == 'priority') {
                    shippingPrice = product.p;
                } else if (shippingType == 'economy') {
                    shippingPrice = product.e;
                } else if (shippingType == 'air') {
                    shippingPrice = product.a;
                }
                if (shippingPrice) $('.item-shipping-' + i).html('$' + shippingPrice);
                var itemTotal = $('.item-total-price-' + i);
                itemTotal.html('$' + (shippingPrice + itemTotal.data('price')).toFixed(2));
            }
        };

    radios.on('stateChanged', changeSummary);
    eraseShippingInformation();
    changeSummary();
    document.querySelector('.js-go-to-payment').addEventListener('click', function (e) {
        var type = $('.js-ship-radio:checked').val();
        var city = select.val();
        if (!type || !city) {
            e.preventDefault();
            alert('Choose shipping option first');

            return 0;
        }
        this.href = 'payment.php?type=' + type + '&cty=' + city;
    })
})();
