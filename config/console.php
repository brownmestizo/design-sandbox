<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once 'config_local.php';

use Console\UpdateCurrencyRatesCommand;
use Currency\ApiConverter;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new UpdateCurrencyRatesCommand(new ApiConverter()));
$application->run();
