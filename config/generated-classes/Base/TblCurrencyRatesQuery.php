<?php

namespace Base;

use \TblCurrencyRates as ChildTblCurrencyRates;
use \TblCurrencyRatesQuery as ChildTblCurrencyRatesQuery;
use \Exception;
use \PDO;
use Map\TblCurrencyRatesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tbl_currency_rates' table.
 *
 *
 *
 * @method     ChildTblCurrencyRatesQuery orderById($order = Criteria::ASC) Order by the rate_id column
 * @method     ChildTblCurrencyRatesQuery orderByCurrency($order = Criteria::ASC) Order by the rate_currency column
 * @method     ChildTblCurrencyRatesQuery orderByRate($order = Criteria::ASC) Order by the rate_to_usd column
 *
 * @method     ChildTblCurrencyRatesQuery groupById() Group by the rate_id column
 * @method     ChildTblCurrencyRatesQuery groupByCurrency() Group by the rate_currency column
 * @method     ChildTblCurrencyRatesQuery groupByRate() Group by the rate_to_usd column
 *
 * @method     ChildTblCurrencyRatesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTblCurrencyRatesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTblCurrencyRatesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTblCurrencyRatesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTblCurrencyRatesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTblCurrencyRatesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTblCurrencyRates findOne(ConnectionInterface $con = null) Return the first ChildTblCurrencyRates matching the query
 * @method     ChildTblCurrencyRates findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTblCurrencyRates matching the query, or a new ChildTblCurrencyRates object populated from the query conditions when no match is found
 *
 * @method     ChildTblCurrencyRates findOneById(int $rate_id) Return the first ChildTblCurrencyRates filtered by the rate_id column
 * @method     ChildTblCurrencyRates findOneByCurrency(string $rate_currency) Return the first ChildTblCurrencyRates filtered by the rate_currency column
 * @method     ChildTblCurrencyRates findOneByRate(double $rate_to_usd) Return the first ChildTblCurrencyRates filtered by the rate_to_usd column *

 * @method     ChildTblCurrencyRates requirePk($key, ConnectionInterface $con = null) Return the ChildTblCurrencyRates by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblCurrencyRates requireOne(ConnectionInterface $con = null) Return the first ChildTblCurrencyRates matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTblCurrencyRates requireOneById(int $rate_id) Return the first ChildTblCurrencyRates filtered by the rate_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblCurrencyRates requireOneByCurrency(string $rate_currency) Return the first ChildTblCurrencyRates filtered by the rate_currency column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblCurrencyRates requireOneByRate(double $rate_to_usd) Return the first ChildTblCurrencyRates filtered by the rate_to_usd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTblCurrencyRates[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTblCurrencyRates objects based on current ModelCriteria
 * @method     ChildTblCurrencyRates[]|ObjectCollection findById(int $rate_id) Return ChildTblCurrencyRates objects filtered by the rate_id column
 * @method     ChildTblCurrencyRates[]|ObjectCollection findByCurrency(string $rate_currency) Return ChildTblCurrencyRates objects filtered by the rate_currency column
 * @method     ChildTblCurrencyRates[]|ObjectCollection findByRate(double $rate_to_usd) Return ChildTblCurrencyRates objects filtered by the rate_to_usd column
 * @method     ChildTblCurrencyRates[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TblCurrencyRatesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TblCurrencyRatesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpm', $modelName = '\\TblCurrencyRates', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTblCurrencyRatesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTblCurrencyRatesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTblCurrencyRatesQuery) {
            return $criteria;
        }
        $query = new ChildTblCurrencyRatesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTblCurrencyRates|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TblCurrencyRatesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TblCurrencyRatesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTblCurrencyRates A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT rate_id, rate_currency, rate_to_usd FROM tbl_currency_rates WHERE rate_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTblCurrencyRates $obj */
            $obj = new ChildTblCurrencyRates();
            $obj->hydrate($row);
            TblCurrencyRatesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTblCurrencyRates|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTblCurrencyRatesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTblCurrencyRatesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the rate_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE rate_id = 1234
     * $query->filterById(array(12, 34)); // WHERE rate_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE rate_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblCurrencyRatesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_ID, $id, $comparison);
    }

    /**
     * Filter the query on the rate_currency column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrency('fooValue');   // WHERE rate_currency = 'fooValue'
     * $query->filterByCurrency('%fooValue%', Criteria::LIKE); // WHERE rate_currency LIKE '%fooValue%'
     * </code>
     *
     * @param     string $currency The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblCurrencyRatesQuery The current query, for fluid interface
     */
    public function filterByCurrency($currency = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($currency)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_CURRENCY, $currency, $comparison);
    }

    /**
     * Filter the query on the rate_to_usd column
     *
     * Example usage:
     * <code>
     * $query->filterByRate(1234); // WHERE rate_to_usd = 1234
     * $query->filterByRate(array(12, 34)); // WHERE rate_to_usd IN (12, 34)
     * $query->filterByRate(array('min' => 12)); // WHERE rate_to_usd > 12
     * </code>
     *
     * @param     mixed $rate The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblCurrencyRatesQuery The current query, for fluid interface
     */
    public function filterByRate($rate = null, $comparison = null)
    {
        if (is_array($rate)) {
            $useMinMax = false;
            if (isset($rate['min'])) {
                $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_TO_USD, $rate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rate['max'])) {
                $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_TO_USD, $rate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_TO_USD, $rate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTblCurrencyRates $tblCurrencyRates Object to remove from the list of results
     *
     * @return $this|ChildTblCurrencyRatesQuery The current query, for fluid interface
     */
    public function prune($tblCurrencyRates = null)
    {
        if ($tblCurrencyRates) {
            $this->addUsingAlias(TblCurrencyRatesTableMap::COL_RATE_ID, $tblCurrencyRates->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tbl_currency_rates table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TblCurrencyRatesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TblCurrencyRatesTableMap::clearInstancePool();
            TblCurrencyRatesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TblCurrencyRatesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TblCurrencyRatesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TblCurrencyRatesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TblCurrencyRatesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TblCurrencyRatesQuery
