<?php

namespace Base;

use \TblProdPhotosv2 as ChildTblProdPhotosv2;
use \TblProdPhotosv2Query as ChildTblProdPhotosv2Query;
use \Exception;
use \PDO;
use Map\TblProdPhotosv2TableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tbl_prod_photosv2' table.
 *
 *
 *
 * @method     ChildTblProdPhotosv2Query orderByProdId($order = Criteria::ASC) Order by the prod_id column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo1($order = Criteria::ASC) Order by the prod_solo_1 column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo2($order = Criteria::ASC) Order by the prod_solo_2 column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo3($order = Criteria::ASC) Order by the prod_solo_3 column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo4($order = Criteria::ASC) Order by the prod_solo_4 column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo1Pa($order = Criteria::ASC) Order by the prod_solo_1_pa column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo2Pa($order = Criteria::ASC) Order by the prod_solo_2_pa column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo3Pa($order = Criteria::ASC) Order by the prod_solo_3_pa column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo4Pa($order = Criteria::ASC) Order by the prod_solo_4_pa column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo1M3($order = Criteria::ASC) Order by the prod_solo_1_m3 column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo2M3($order = Criteria::ASC) Order by the prod_solo_2_m3 column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo3M3($order = Criteria::ASC) Order by the prod_solo_3_m3 column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo4M3($order = Criteria::ASC) Order by the prod_solo_4_m3 column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo1Blank($order = Criteria::ASC) Order by the prod_solo_1_blank column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo2Blank($order = Criteria::ASC) Order by the prod_solo_2_blank column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo3Blank($order = Criteria::ASC) Order by the prod_solo_3_blank column
 * @method     ChildTblProdPhotosv2Query orderByProdSolo4Blank($order = Criteria::ASC) Order by the prod_solo_4_blank column
 *
 * @method     ChildTblProdPhotosv2Query groupByProdId() Group by the prod_id column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo1() Group by the prod_solo_1 column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo2() Group by the prod_solo_2 column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo3() Group by the prod_solo_3 column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo4() Group by the prod_solo_4 column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo1Pa() Group by the prod_solo_1_pa column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo2Pa() Group by the prod_solo_2_pa column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo3Pa() Group by the prod_solo_3_pa column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo4Pa() Group by the prod_solo_4_pa column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo1M3() Group by the prod_solo_1_m3 column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo2M3() Group by the prod_solo_2_m3 column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo3M3() Group by the prod_solo_3_m3 column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo4M3() Group by the prod_solo_4_m3 column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo1Blank() Group by the prod_solo_1_blank column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo2Blank() Group by the prod_solo_2_blank column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo3Blank() Group by the prod_solo_3_blank column
 * @method     ChildTblProdPhotosv2Query groupByProdSolo4Blank() Group by the prod_solo_4_blank column
 *
 * @method     ChildTblProdPhotosv2Query leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTblProdPhotosv2Query rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTblProdPhotosv2Query innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTblProdPhotosv2Query leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTblProdPhotosv2Query rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTblProdPhotosv2Query innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTblProdPhotosv2Query leftJoinTblProdInfo($relationAlias = null) Adds a LEFT JOIN clause to the query using the TblProdInfo relation
 * @method     ChildTblProdPhotosv2Query rightJoinTblProdInfo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TblProdInfo relation
 * @method     ChildTblProdPhotosv2Query innerJoinTblProdInfo($relationAlias = null) Adds a INNER JOIN clause to the query using the TblProdInfo relation
 *
 * @method     ChildTblProdPhotosv2Query joinWithTblProdInfo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TblProdInfo relation
 *
 * @method     ChildTblProdPhotosv2Query leftJoinWithTblProdInfo() Adds a LEFT JOIN clause and with to the query using the TblProdInfo relation
 * @method     ChildTblProdPhotosv2Query rightJoinWithTblProdInfo() Adds a RIGHT JOIN clause and with to the query using the TblProdInfo relation
 * @method     ChildTblProdPhotosv2Query innerJoinWithTblProdInfo() Adds a INNER JOIN clause and with to the query using the TblProdInfo relation
 *
 * @method     \TblProdInfoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTblProdPhotosv2 findOne(ConnectionInterface $con = null) Return the first ChildTblProdPhotosv2 matching the query
 * @method     ChildTblProdPhotosv2 findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTblProdPhotosv2 matching the query, or a new ChildTblProdPhotosv2 object populated from the query conditions when no match is found
 *
 * @method     ChildTblProdPhotosv2 findOneByProdId(int $prod_id) Return the first ChildTblProdPhotosv2 filtered by the prod_id column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo1(string $prod_solo_1) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_1 column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo2(string $prod_solo_2) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_2 column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo3(string $prod_solo_3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_3 column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo4(string $prod_solo_4) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_4 column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo1Pa(string $prod_solo_1_pa) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_1_pa column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo2Pa(string $prod_solo_2_pa) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_2_pa column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo3Pa(string $prod_solo_3_pa) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_3_pa column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo4Pa(string $prod_solo_4_pa) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_4_pa column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo1M3(string $prod_solo_1_m3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_1_m3 column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo2M3(string $prod_solo_2_m3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_2_m3 column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo3M3(string $prod_solo_3_m3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_3_m3 column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo4M3(string $prod_solo_4_m3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_4_m3 column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo1Blank(string $prod_solo_1_blank) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_1_blank column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo2Blank(string $prod_solo_2_blank) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_2_blank column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo3Blank(string $prod_solo_3_blank) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_3_blank column
 * @method     ChildTblProdPhotosv2 findOneByProdSolo4Blank(string $prod_solo_4_blank) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_4_blank column *

 * @method     ChildTblProdPhotosv2 requirePk($key, ConnectionInterface $con = null) Return the ChildTblProdPhotosv2 by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOne(ConnectionInterface $con = null) Return the first ChildTblProdPhotosv2 matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTblProdPhotosv2 requireOneByProdId(int $prod_id) Return the first ChildTblProdPhotosv2 filtered by the prod_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo1(string $prod_solo_1) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo2(string $prod_solo_2) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo3(string $prod_solo_3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_3 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo4(string $prod_solo_4) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_4 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo1Pa(string $prod_solo_1_pa) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_1_pa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo2Pa(string $prod_solo_2_pa) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_2_pa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo3Pa(string $prod_solo_3_pa) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_3_pa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo4Pa(string $prod_solo_4_pa) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_4_pa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo1M3(string $prod_solo_1_m3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_1_m3 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo2M3(string $prod_solo_2_m3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_2_m3 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo3M3(string $prod_solo_3_m3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_3_m3 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo4M3(string $prod_solo_4_m3) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_4_m3 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo1Blank(string $prod_solo_1_blank) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_1_blank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo2Blank(string $prod_solo_2_blank) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_2_blank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo3Blank(string $prod_solo_3_blank) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_3_blank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTblProdPhotosv2 requireOneByProdSolo4Blank(string $prod_solo_4_blank) Return the first ChildTblProdPhotosv2 filtered by the prod_solo_4_blank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTblProdPhotosv2[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTblProdPhotosv2 objects based on current ModelCriteria
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdId(int $prod_id) Return ChildTblProdPhotosv2 objects filtered by the prod_id column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo1(string $prod_solo_1) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_1 column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo2(string $prod_solo_2) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_2 column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo3(string $prod_solo_3) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_3 column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo4(string $prod_solo_4) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_4 column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo1Pa(string $prod_solo_1_pa) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_1_pa column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo2Pa(string $prod_solo_2_pa) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_2_pa column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo3Pa(string $prod_solo_3_pa) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_3_pa column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo4Pa(string $prod_solo_4_pa) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_4_pa column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo1M3(string $prod_solo_1_m3) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_1_m3 column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo2M3(string $prod_solo_2_m3) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_2_m3 column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo3M3(string $prod_solo_3_m3) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_3_m3 column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo4M3(string $prod_solo_4_m3) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_4_m3 column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo1Blank(string $prod_solo_1_blank) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_1_blank column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo2Blank(string $prod_solo_2_blank) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_2_blank column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo3Blank(string $prod_solo_3_blank) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_3_blank column
 * @method     ChildTblProdPhotosv2[]|ObjectCollection findByProdSolo4Blank(string $prod_solo_4_blank) Return ChildTblProdPhotosv2 objects filtered by the prod_solo_4_blank column
 * @method     ChildTblProdPhotosv2[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TblProdPhotosv2Query extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TblProdPhotosv2Query object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpm', $modelName = '\\TblProdPhotosv2', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTblProdPhotosv2Query object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTblProdPhotosv2Query
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTblProdPhotosv2Query) {
            return $criteria;
        }
        $query = new ChildTblProdPhotosv2Query();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTblProdPhotosv2|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TblProdPhotosv2TableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TblProdPhotosv2TableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTblProdPhotosv2 A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT prod_id, prod_solo_1, prod_solo_2, prod_solo_3, prod_solo_4, prod_solo_1_pa, prod_solo_2_pa, prod_solo_3_pa, prod_solo_4_pa, prod_solo_1_m3, prod_solo_2_m3, prod_solo_3_m3, prod_solo_4_m3, prod_solo_1_blank, prod_solo_2_blank, prod_solo_3_blank, prod_solo_4_blank FROM tbl_prod_photosv2 WHERE prod_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTblProdPhotosv2 $obj */
            $obj = new ChildTblProdPhotosv2();
            $obj->hydrate($row);
            TblProdPhotosv2TableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTblProdPhotosv2|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the prod_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProdId(1234); // WHERE prod_id = 1234
     * $query->filterByProdId(array(12, 34)); // WHERE prod_id IN (12, 34)
     * $query->filterByProdId(array('min' => 12)); // WHERE prod_id > 12
     * </code>
     *
     * @see       filterByTblProdInfo()
     *
     * @param     mixed $prodId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdId($prodId = null, $comparison = null)
    {
        if (is_array($prodId)) {
            $useMinMax = false;
            if (isset($prodId['min'])) {
                $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_ID, $prodId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prodId['max'])) {
                $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_ID, $prodId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_ID, $prodId, $comparison);
    }

    /**
     * Filter the query on the prod_solo_1 column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo1('fooValue');   // WHERE prod_solo_1 = 'fooValue'
     * $query->filterByProdSolo1('%fooValue%', Criteria::LIKE); // WHERE prod_solo_1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo1 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo1($prodSolo1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo1)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_1, $prodSolo1, $comparison);
    }

    /**
     * Filter the query on the prod_solo_2 column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo2('fooValue');   // WHERE prod_solo_2 = 'fooValue'
     * $query->filterByProdSolo2('%fooValue%', Criteria::LIKE); // WHERE prod_solo_2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo2 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo2($prodSolo2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo2)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_2, $prodSolo2, $comparison);
    }

    /**
     * Filter the query on the prod_solo_3 column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo3('fooValue');   // WHERE prod_solo_3 = 'fooValue'
     * $query->filterByProdSolo3('%fooValue%', Criteria::LIKE); // WHERE prod_solo_3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo3 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo3($prodSolo3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo3)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_3, $prodSolo3, $comparison);
    }

    /**
     * Filter the query on the prod_solo_4 column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo4('fooValue');   // WHERE prod_solo_4 = 'fooValue'
     * $query->filterByProdSolo4('%fooValue%', Criteria::LIKE); // WHERE prod_solo_4 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo4 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo4($prodSolo4 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo4)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_4, $prodSolo4, $comparison);
    }

    /**
     * Filter the query on the prod_solo_1_pa column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo1Pa('fooValue');   // WHERE prod_solo_1_pa = 'fooValue'
     * $query->filterByProdSolo1Pa('%fooValue%', Criteria::LIKE); // WHERE prod_solo_1_pa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo1Pa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo1Pa($prodSolo1Pa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo1Pa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_1_PA, $prodSolo1Pa, $comparison);
    }

    /**
     * Filter the query on the prod_solo_2_pa column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo2Pa('fooValue');   // WHERE prod_solo_2_pa = 'fooValue'
     * $query->filterByProdSolo2Pa('%fooValue%', Criteria::LIKE); // WHERE prod_solo_2_pa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo2Pa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo2Pa($prodSolo2Pa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo2Pa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_2_PA, $prodSolo2Pa, $comparison);
    }

    /**
     * Filter the query on the prod_solo_3_pa column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo3Pa('fooValue');   // WHERE prod_solo_3_pa = 'fooValue'
     * $query->filterByProdSolo3Pa('%fooValue%', Criteria::LIKE); // WHERE prod_solo_3_pa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo3Pa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo3Pa($prodSolo3Pa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo3Pa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_3_PA, $prodSolo3Pa, $comparison);
    }

    /**
     * Filter the query on the prod_solo_4_pa column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo4Pa('fooValue');   // WHERE prod_solo_4_pa = 'fooValue'
     * $query->filterByProdSolo4Pa('%fooValue%', Criteria::LIKE); // WHERE prod_solo_4_pa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo4Pa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo4Pa($prodSolo4Pa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo4Pa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_4_PA, $prodSolo4Pa, $comparison);
    }

    /**
     * Filter the query on the prod_solo_1_m3 column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo1M3('fooValue');   // WHERE prod_solo_1_m3 = 'fooValue'
     * $query->filterByProdSolo1M3('%fooValue%', Criteria::LIKE); // WHERE prod_solo_1_m3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo1M3 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo1M3($prodSolo1M3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo1M3)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_1_M3, $prodSolo1M3, $comparison);
    }

    /**
     * Filter the query on the prod_solo_2_m3 column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo2M3('fooValue');   // WHERE prod_solo_2_m3 = 'fooValue'
     * $query->filterByProdSolo2M3('%fooValue%', Criteria::LIKE); // WHERE prod_solo_2_m3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo2M3 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo2M3($prodSolo2M3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo2M3)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_2_M3, $prodSolo2M3, $comparison);
    }

    /**
     * Filter the query on the prod_solo_3_m3 column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo3M3('fooValue');   // WHERE prod_solo_3_m3 = 'fooValue'
     * $query->filterByProdSolo3M3('%fooValue%', Criteria::LIKE); // WHERE prod_solo_3_m3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo3M3 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo3M3($prodSolo3M3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo3M3)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_3_M3, $prodSolo3M3, $comparison);
    }

    /**
     * Filter the query on the prod_solo_4_m3 column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo4M3('fooValue');   // WHERE prod_solo_4_m3 = 'fooValue'
     * $query->filterByProdSolo4M3('%fooValue%', Criteria::LIKE); // WHERE prod_solo_4_m3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo4M3 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo4M3($prodSolo4M3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo4M3)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_4_M3, $prodSolo4M3, $comparison);
    }

    /**
     * Filter the query on the prod_solo_1_blank column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo1Blank('fooValue');   // WHERE prod_solo_1_blank = 'fooValue'
     * $query->filterByProdSolo1Blank('%fooValue%', Criteria::LIKE); // WHERE prod_solo_1_blank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo1Blank The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo1Blank($prodSolo1Blank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo1Blank)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_1_BLANK, $prodSolo1Blank, $comparison);
    }

    /**
     * Filter the query on the prod_solo_2_blank column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo2Blank('fooValue');   // WHERE prod_solo_2_blank = 'fooValue'
     * $query->filterByProdSolo2Blank('%fooValue%', Criteria::LIKE); // WHERE prod_solo_2_blank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo2Blank The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo2Blank($prodSolo2Blank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo2Blank)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_2_BLANK, $prodSolo2Blank, $comparison);
    }

    /**
     * Filter the query on the prod_solo_3_blank column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo3Blank('fooValue');   // WHERE prod_solo_3_blank = 'fooValue'
     * $query->filterByProdSolo3Blank('%fooValue%', Criteria::LIKE); // WHERE prod_solo_3_blank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo3Blank The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo3Blank($prodSolo3Blank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo3Blank)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_3_BLANK, $prodSolo3Blank, $comparison);
    }

    /**
     * Filter the query on the prod_solo_4_blank column
     *
     * Example usage:
     * <code>
     * $query->filterByProdSolo4Blank('fooValue');   // WHERE prod_solo_4_blank = 'fooValue'
     * $query->filterByProdSolo4Blank('%fooValue%', Criteria::LIKE); // WHERE prod_solo_4_blank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prodSolo4Blank The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByProdSolo4Blank($prodSolo4Blank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prodSolo4Blank)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_SOLO_4_BLANK, $prodSolo4Blank, $comparison);
    }

    /**
     * Filter the query by a related \TblProdInfo object
     *
     * @param \TblProdInfo|ObjectCollection $tblProdInfo The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function filterByTblProdInfo($tblProdInfo, $comparison = null)
    {
        if ($tblProdInfo instanceof \TblProdInfo) {
            return $this
                ->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_ID, $tblProdInfo->getProdId(), $comparison);
        } elseif ($tblProdInfo instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_ID, $tblProdInfo->toKeyValue('PrimaryKey', 'ProdId'), $comparison);
        } else {
            throw new PropelException('filterByTblProdInfo() only accepts arguments of type \TblProdInfo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TblProdInfo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function joinTblProdInfo($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TblProdInfo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TblProdInfo');
        }

        return $this;
    }

    /**
     * Use the TblProdInfo relation TblProdInfo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TblProdInfoQuery A secondary query class using the current class as primary query
     */
    public function useTblProdInfoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTblProdInfo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TblProdInfo', '\TblProdInfoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTblProdPhotosv2 $tblProdPhotosv2 Object to remove from the list of results
     *
     * @return $this|ChildTblProdPhotosv2Query The current query, for fluid interface
     */
    public function prune($tblProdPhotosv2 = null)
    {
        if ($tblProdPhotosv2) {
            $this->addUsingAlias(TblProdPhotosv2TableMap::COL_PROD_ID, $tblProdPhotosv2->getProdId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tbl_prod_photosv2 table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TblProdPhotosv2TableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TblProdPhotosv2TableMap::clearInstancePool();
            TblProdPhotosv2TableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TblProdPhotosv2TableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TblProdPhotosv2TableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TblProdPhotosv2TableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TblProdPhotosv2TableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TblProdPhotosv2Query
