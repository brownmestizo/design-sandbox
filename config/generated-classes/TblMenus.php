<?php

use Base\TblMenus as BaseTblMenus;

/**
 * Skeleton subclass for representing a row from the 'tbl_menus' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TblMenus extends BaseTblMenus
{

    /**
     * Get the product count for each category under each website.
     *
     * @return count
     */	
	public function getProductCount ($website) {

		$productCount = 0;

		switch($website) {
			case 'mb':
				$productCount = TblProdInfoQuery::create()
					->filterByMb(true)
					->filterByProdCategory($this->menu_alias)
					->count();
			break;

			case 'm3':
				$productCount = TblProdInfoQuery::create()
					->filterByM3(true)
					->filterByProdCategory($this->menu_alias)
					->count();			
			break;

			case 'pa':
				$productCount = TblProdInfoQuery::create()
					->filterByPa(true)
					->filterByProdCategory($this->menu_alias)
					->count();			
			break;
		}

		return $productCount;
	}

    /**
     * Get the [menu_writeup] column value.
     *
     * @return string
     */
    public function getMenuWriteup()
    {
        return html_entity_decode($this->menu_writeup);
    }


    /**
     * Get the [menu_writeup] column value.
     *
     * @return string
     */
    public function getMenuWriteupPa()
    {
        return html_entity_decode($this->menu_writeup_pa);
    }


    /**
     * Get the [menu_writeup] column value.
     *
     * @return string
     */
    public function getMenuWriteupM3()
    {
        return html_entity_decode($this->menu_writeup_m3);
    }    



    public function generateTitle ($website) {

        $name = $this->getMenuName()." ".$this->getMenuTitle();

        switch($website) {
            case 'mb':
                $title = 'Modelbuffs '.$name;
            break;

            case 'm3':
                $title = 'MyMahoganyModels '.$name;
            break;

            case 'pa':
                $title = 'Planearts '.$name;            
            break;
        }

        return $title;         
    }

    public function generateSEOKeywords ($website) {
        
        $keywords = "";

        switch($website) {
            case 'mb':
                $keywords = $this->getMenuStatus();
            break;

            case 'm3':
                $keywords = $this->getMenuStatusM3();
            break;

            case 'pa':
                $keywords = $this->getMenuStatusPa();
            break;
        }         

        return $keywords;
    }


    public function generateSEODescription ($website) {

        $description = "";

        switch($website) {
            case 'mb':
                $keywords = $this->getMenuDescription();
            break;

            case 'm3':
                $keywords = $this->getMenuDescriptionM3();
            break;

            case 'pa':
                $keywords = $this->getMenuDescriptionPa();
            break;
        } 
        
        return $description;

    }

}
