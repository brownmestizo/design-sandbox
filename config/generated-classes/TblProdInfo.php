<?php

use Base\TblProdInfo as BaseTblProdInfo;
use Map\TblProdInfoTableMap;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'tbl_prod_info' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TblProdInfo extends BaseTblProdInfo
{
    /** @var \Form\UploadedPhotos|null */
    private $photos;

    /** @var \Form\UploadedPhotos|null */
    private $photosPa;

    /** @var \Form\UploadedPhotos|null */
    private $photosM3;

    public function getProdWriteup() {    	    	    	    	
    	$output = preg_replace('/\+/', "", preg_replace('/\+\./', "", $this->prod_writeup));
    	return $output;
    }

    public function numberFormat ($x) {
    	return number_format($x, 2);
    }

    public function numberConvertToMetric ($x) {
    	return ($x * 2.54);
    }

    public function displayStat ($x, $j, $raw=null) {
        if ($x != "") {
        	$formatted = number_format((float)$x, 2);
        	$metric = number_format(((float)$x * 2.54), 2);

            if ($raw==null) 
                $label = 
                    '<span><strong>'.$j.'</strong> '.
                    $formatted." in (".$metric." cm)".
                    '</span>';
            else 
                $label = 
                    $j.': '.$formatted." in (".$metric." cm)";
        } else {
            $label = "";
        }           

    	return $label;
    }

    public function setProdAlt($altsStr)
    {
        $this->prod_alt1 = '';
        $this->prod_alt2 = '';
        $this->prod_alt3 = '';
        $this->prod_alt4 = '';
        $alts = explode(',', $altsStr);
        if (isset($alts[0])) {
            $this->prod_alt1 = trim($alts[0]);
        }
        if (isset($alts[1])) {
            $this->prod_alt2 = trim($alts[1]);
        }
        if (isset($alts[2])) {
            $this->prod_alt3 = trim($alts[2]);
        }
        if (isset($alts[3])) {
            $this->prod_alt4 = trim($alts[3]);
        }
        $this->modifiedColumns[TblProdInfoTableMap::COL_PROD_ALT1] = true;
        $this->modifiedColumns[TblProdInfoTableMap::COL_PROD_ALT2] = true;
        $this->modifiedColumns[TblProdInfoTableMap::COL_PROD_ALT3] = true;
        $this->modifiedColumns[TblProdInfoTableMap::COL_PROD_ALT4] = true;
    }

    public function getProdAlt()
    {
        $res = trim(str_replace(',,', ',',
            implode(',', [$this->prod_alt1, $this->prod_alt2, $this->prod_alt3, $this->prod_alt4])), ',');

        return $res;
    }

    public function postInsert(ConnectionInterface $con = null)
    {
        $photos = $this->photos->getTblPhotos($this, '');
        $photos->save($con);

        $photos = $this->photosPa->getTblPhotos($this, 'Pa');
        $photos->save($con);

        $photos = $this->photosM3->getTblPhotos($this, 'M3');
        $photos->save($con);
    }

    public function preUpdate(ConnectionInterface $con = null)
    {
        $photos = $this->photos->getTblPhotos($this, '');
        $photos->save($con);

        $photos = $this->photosPa->getTblPhotos($this, 'Pa');
        $photos->save($con);

        $photos = $this->photosM3->getTblPhotos($this, 'M3');
        $photos->save($con);
        
        return true;
    }

    /**
     * @return mixed
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param mixed $photos
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
    }

    /**
     * @return mixed
     */
    public function getPhotosPa()
    {
        return $this->photosPa;
    }

    /**
     * @param mixed $photos
     */
    public function setPhotosPa($photos)
    {
        $this->photosPa = $photos;
    }

    /**
     * @return mixed
     */
    public function getPhotosM3()
    {
        return $this->photosM3;
    }

    /**
     * @param mixed $photos
     */
    public function setPhotosM3($photos)
    {
        $this->photosM3 = $photos;
    }

    public function getRelatedProductsIds()
    {
        return explode(" ", $this->getProdRelated());
    }

    public function generateTitle ($website) {

        $extraTitle = "";

        if ($this->getProdTitle()) {
            $extraTitle = " ".$this->getProdTitle();
        }

        $name = $this->getProdName().$extraTitle;

        if ( $this->getProdPriceId() > 0 )  
            $price = '$'.number_format($this->getTblProdPricing()->getProdPricePrice(), 2);
        else 
            $price = '$'.number_format($this->getTblProdPrices()->getProdNormalPrice(), 2);        

        switch($website) {
            case 'mb':
                $title = $name.' Modelbuffs Custom Made Mahogany Models';
            break;

            case 'm3':
                $title = $name.' '.$this->getTblMenus()->getMenuName().' '.$price.' MyMahoganyModels '.$this->getTblMenus()->getMenuTitle();     
            break;

            case 'pa':
                $title = $name.' '.$this->getTblMenus()->getMenuName().' '.$price.' Planearts '.$this->getTblMenus()->getMenuTitle();            
            break;
        }

        return $title;         
    }

    public function generateSEOKeywords ($website) {

        $holder = array(
                $this->getProdName(),
                $this->getTblMenus()->getMenuName(),
                $this->getTblEra()->getEraDescription(),
                $this->getProdCompany(),
                $this->getProdKeywords(),
                $this->getProdKeywordsWriteup(),
                $this->getProdAlt1(),
                $this->getProdAlt2(),
                $this->getProdAlt3(),
                $this->getProdAlt4(),
            );

        
        switch($website) {
            case 'mb':
                $holder[] = $this->getTblGeneral()->getProdKeywords();
                $holder[] = $this->getTblGeneral()->getProdWriteupKeywords();
            break;

            case 'm3':
                $holder[] = $this->getTblGeneral()->getProdKeywordsM3();
                $holder[] = $this->getTblGeneral()->getProdWriteupKeywordsM3();
            break;

            case 'pa':
                $holder[] = $this->getTblGeneral()->getProdKeywordsPa();
                $holder[] = $this->getTblGeneral()->getProdWriteupKeywordsPa();
            break;
        }         

        $keywords = "";
        $ctr = 0; $length = count($holder);

        foreach($holder as $x) {        
            if ($x != "") {
                $keywords .= $x;


                if ($ctr != $length-1) {
                    $keywords .= ",";
                }

            }

            $ctr++;
        }

        return $keywords;
    }


    public function generateSEODescription ($websiteName) {

        $holder = array(
                $this->getProdName(),
                $this->getProdAlt1(),
                $this->getProdAlt2(),
                $this->getProdAlt3(),
                $this->getProdAlt4(),
                $this->getTblMenus()->getMenuName().' model',
                $this->getProdCode(),

                $this->displayStat($this->getProdLength(), 'Body Length', 1),
                $this->displayStat($this->getProdWingSpan(), 'Wing Span', 1),
                $this->displayStat($this->getProdHeight(), 'Height', 1),
                $this->displayStat($this->getProdScale(), 'Scale', 1),


                $this->getTblEra()->getEraDescription(),
                "Company: ".$this->getProdCompany(),
                $this->getProdDescription(),
                " manufactured by ".$websiteName." Company",
                $this->getTblMenus()->getMenuDescription(),
            );

        $description = "";

        foreach($holder as $x) {        
            if ($x != "") {
                $description .= $x." ";
            }
        }
        
        return $description;

    }

    public function displayMainImage($website) {
    	
        switch($website) {
            case 'mb':
                $image = rawurlencode($this->getTblProdPhotosv2()->getProdSolo1());
            break;

            case 'pa':
                $image = rawurlencode($this->getTblProdPhotosv2()->getProdSolo1Pa());                                      
            break;

            case 'm3':
                $image = rawurlencode($this->getTblProdPhotosv2()->getProdSolo1M3());
            break;
        }        

        return $image;
    }

    public function displayGallery($website) {
        $gallery = array();

        switch($website) {
            case 'mb':
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo2());
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo3());
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo4());
            break;

            case 'pa':
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo2Pa());
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo3Pa());
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo4Pa());                                                
            break;

            case 'm3':
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo2M3());
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo3M3());
                $gallery[] = rawurlencode($this->getTblProdPhotosv2()->getProdSolo4M3());
            break;
        }  

        return $gallery;
    }

}
