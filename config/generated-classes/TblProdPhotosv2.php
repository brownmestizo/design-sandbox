<?php

use Base\TblProdPhotosv2 as BaseTblProdPhotosv2;

/**
 * Skeleton subclass for representing a row from the 'tbl_prod_photosv2' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TblProdPhotosv2 extends BaseTblProdPhotosv2
{
    public function __construct()
    {
        $this->setProdSolo1('');
        $this->setProdSolo2('');
        $this->setProdSolo3('');
        $this->setProdSolo4('');
        parent::__construct();
    }
}
