<?php

use Base\TblProdPhotosv2Query as BaseTblProdPhotosv2Query;

/**
 * Skeleton subclass for performing query and update operations on the 'tbl_prod_photosv2' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TblProdPhotosv2Query extends BaseTblProdPhotosv2Query
{

}
