<?php
use Propel\Generator\Manager\MigrationManager;

require_once 'vendor/autoload.php';
include 'config/propel.php';

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1477899620.
 * Generated on 2016-10-31 07:40:20 by vagrant
 */
// bootstrap the Propel runtime (and other dependencies)


class PropelMigration_1477899620
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
        // post-migration code
        $sql = array();
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1222 WHERE cty_id = 2";
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1224 WHERE cty_id = 3";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1081 WHERE cty_id = 4";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1506 WHERE cty_id = 5";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1209 WHERE cty_id = 6";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1427 WHERE cty_id = 7";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1521 WHERE cty_id = 9";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1364 WHERE cty_id = 10";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1322 WHERE cty_id = 11";    

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1110 WHERE cty_id = 12";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1381 WHERE cty_id = 13";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1116 WHERE cty_id = 15";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 982 WHERE cty_id = 16";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 913 WHERE cty_id = 17";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1107 WHERE cty_id = 18";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1242 WHERE cty_id = 20";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1885 WHERE cty_id = 21";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1381 WHERE cty_id = 22";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1060 WHERE cty_id = 23";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1081 WHERE cty_id = 24";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 909 WHERE cty_id = 25";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1949 WHERE cty_id = 26";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1461 WHERE cty_id = 29";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1454 WHERE cty_id = 30";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 718 WHERE cty_id = 32";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1144 WHERE cty_id = 33";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1240 WHERE cty_id = 34";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1250 WHERE cty_id = 35";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 783 WHERE cty_id = 36";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1083 WHERE cty_id = 37";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1277 WHERE cty_id = 38";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1389 WHERE cty_id = 39";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1125 WHERE cty_id = 40";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1264 WHERE cty_id = 41";    

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1142 WHERE cty_id = 42";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1453 WHERE cty_id = 44";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 822 WHERE cty_id = 45";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1567 WHERE cty_id = 48";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1279 WHERE cty_id = 50";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1010 WHERE cty_id = 49";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1381 WHERE cty_id = 51";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1551 WHERE cty_id = 52";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1132 WHERE cty_id = 53";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1122 WHERE cty_id = 55";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1225 WHERE cty_id = 56";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1356 WHERE cty_id = 57";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1022 WHERE cty_id = 58";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1044 WHERE cty_id = 60";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1434 WHERE cty_id = 64";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1428 WHERE cty_id = 66";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 998 WHERE cty_id = 68";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1285 WHERE cty_id = 69";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1183 WHERE cty_id = 70";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1052 WHERE cty_id = 71";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1377 WHERE cty_id = 72";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 962 WHERE cty_id = 73";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1651 WHERE cty_id = 74";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1504 WHERE cty_id = 75";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1543 WHERE cty_id = 76";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1254 WHERE cty_id = 77";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1239 WHERE cty_id = 78";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1642 WHERE cty_id = 81";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1198 WHERE cty_id = 82";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1264 WHERE cty_id = 87";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1372 WHERE cty_id = 88";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1110 WHERE cty_id = 89";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1543 WHERE cty_id = 90";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1091 WHERE cty_id = 93";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1298 WHERE cty_id = 95";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1132 WHERE cty_id = 97";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 893 WHERE cty_id = 98";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1462 WHERE cty_id = 99";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1558 WHERE cty_id = 100";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1070 WHERE cty_id = 101";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 786 WHERE cty_id = 102";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1405 WHERE cty_id = 104";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1360 WHERE cty_id = 105";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1320 WHERE cty_id = 106";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1071 WHERE cty_id = 109";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1268 WHERE cty_id = 110";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1000 WHERE cty_id = 112";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1297 WHERE cty_id = 113";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1193 WHERE cty_id = 114";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 845 WHERE cty_id = 115";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 953 WHERE cty_id = 116";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1022 WHERE cty_id = 117";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1243 WHERE cty_id = 118";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1242 WHERE cty_id = 119";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1025 WHERE cty_id = 120";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1230 WHERE cty_id = 122";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1121 WHERE cty_id = 124";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 858 WHERE cty_id = 125";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1264 WHERE cty_id = 127";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 883 WHERE cty_id = 129";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 950 WHERE cty_id = 130";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1174 WHERE cty_id = 131";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1357 WHERE cty_id = 132";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1133 WHERE cty_id = 133";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1427 WHERE cty_id = 134";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1273 WHERE cty_id = 135";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1253 WHERE cty_id = 136";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1182 WHERE cty_id = 137";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1133 WHERE cty_id = 138";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1504 WHERE cty_id = 140";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 993 WHERE cty_id = 141";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1278 WHERE cty_id = 143";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1119 WHERE cty_id = 144";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1415 WHERE cty_id = 145";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 840 WHERE cty_id = 147";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1345 WHERE cty_id = 148";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1310 WHERE cty_id = 151";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1134 WHERE cty_id = 152";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1534 WHERE cty_id = 155";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1081 WHERE cty_id = 156";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1709 WHERE cty_id = 159";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1009 WHERE cty_id = 160";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1116 WHERE cty_id = 161";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1133 WHERE cty_id = 162";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1081 WHERE cty_id = 164"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1249 WHERE cty_id = 165";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1341 WHERE cty_id = 166";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1326 WHERE cty_id = 167";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1357 WHERE cty_id = 168";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1537 WHERE cty_id = 169"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1427 WHERE cty_id = 170";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 878 WHERE cty_id = 171";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1513 WHERE cty_id = 172";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1384 WHERE cty_id = 173";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1535 WHERE cty_id = 175";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1133 WHERE cty_id = 178";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1320 WHERE cty_id = 180";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1005 WHERE cty_id = 181";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1132 WHERE cty_id = 184";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1113 WHERE cty_id = 182";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 792 WHERE cty_id = 185";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1205 WHERE cty_id = 186";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1389 WHERE cty_id = 190";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 928 WHERE cty_id = 191";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1188 WHERE cty_id = 202";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1161 WHERE cty_id = 203";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1097 WHERE cty_id = 204";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1565 WHERE cty_id = 205";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1230 WHERE cty_id = 206";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1187 WHERE cty_id = 207";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 752 WHERE cty_id = 209";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1206 WHERE cty_id = 210";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 817 WHERE cty_id = 211";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1252 WHERE cty_id = 213";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 861 WHERE cty_id = 214";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1056 WHERE cty_id = 217"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1121 WHERE cty_id = 218";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 865 WHERE cty_id = 219";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1028 WHERE cty_id = 220";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1176 WHERE cty_id = 232";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1474 WHERE cty_id = 233"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 964 WHERE cty_id = 221";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1627 WHERE cty_id = 236";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1427 WHERE cty_id = 223";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1318 WHERE cty_id = 237";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 836 WHERE cty_id = 238"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1147 WHERE cty_id = 239"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1205 WHERE cty_id = 240";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 763 WHERE cty_id = 241";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1427 WHERE cty_id = 242";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1070 WHERE cty_id = 243";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1085 WHERE cty_id = 244"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1242 WHERE cty_id = 246";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_fkg = 1424 WHERE cty_id = 247";




        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 430 WHERE cty_id = 2";
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 438 WHERE cty_id = 3";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 352 WHERE cty_id = 4";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 467 WHERE cty_id = 5";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 461 WHERE cty_id = 6";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 654 WHERE cty_id = 7";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 614 WHERE cty_id = 9";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 456 WHERE cty_id = 10";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 617 WHERE cty_id = 11";    

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 379 WHERE cty_id = 12";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 438 WHERE cty_id = 13";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 503 WHERE cty_id = 15";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 344 WHERE cty_id = 16";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 304 WHERE cty_id = 17";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 497 WHERE cty_id = 18";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 496 WHERE cty_id = 20";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 463 WHERE cty_id = 21";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 537 WHERE cty_id = 22";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 469 WHERE cty_id = 23";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 469 WHERE cty_id = 24";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 196 WHERE cty_id = 25";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 940 WHERE cty_id = 26";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 502 WHERE cty_id = 29";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 719 WHERE cty_id = 30";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 201 WHERE cty_id = 32";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 426 WHERE cty_id = 33";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 472 WHERE cty_id = 34";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 433 WHERE cty_id = 35";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 705 WHERE cty_id = 36";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 415 WHERE cty_id = 37";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 445 WHERE cty_id = 38";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 427 WHERE cty_id = 39";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 425 WHERE cty_id = 40";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 549 WHERE cty_id = 41";    

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 480 WHERE cty_id = 42";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 638 WHERE cty_id = 44";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 244 WHERE cty_id = 45";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 775 WHERE cty_id = 48";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 506 WHERE cty_id = 50";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 415 WHERE cty_id = 49";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 417 WHERE cty_id = 51";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 587 WHERE cty_id = 52";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 427 WHERE cty_id = 53";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 340 WHERE cty_id = 55";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 446 WHERE cty_id = 56";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 402 WHERE cty_id = 57";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 383 WHERE cty_id = 58";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 492 WHERE cty_id = 60";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 538 WHERE cty_id = 64";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 613 WHERE cty_id = 66";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 464 WHERE cty_id = 68";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 361 WHERE cty_id = 69";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 333 WHERE cty_id = 70";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 437 WHERE cty_id = 71";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 417 WHERE cty_id = 72";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 296 WHERE cty_id = 73";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 429 WHERE cty_id = 74";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 467 WHERE cty_id = 75";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 502 WHERE cty_id = 76";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 363 WHERE cty_id = 77";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 363 WHERE cty_id = 78";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 398 WHERE cty_id = 81";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 435 WHERE cty_id = 82";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 471 WHERE cty_id = 87";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 417 WHERE cty_id = 88";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 497 WHERE cty_id = 89";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 502 WHERE cty_id = 90";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 420 WHERE cty_id = 93";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 528 WHERE cty_id = 95";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 516 WHERE cty_id = 97";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 168 WHERE cty_id = 98";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 480 WHERE cty_id = 99";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 452 WHERE cty_id = 100";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 292 WHERE cty_id = 101";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 254 WHERE cty_id = 102";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 452 WHERE cty_id = 104";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 453 WHERE cty_id = 105";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 433 WHERE cty_id = 106";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 256 WHERE cty_id = 109";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 571 WHERE cty_id = 110";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 297 WHERE cty_id = 112";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 494 WHERE cty_id = 113";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 505 WHERE cty_id = 114";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 264 WHERE cty_id = 115";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 260 WHERE cty_id = 116";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 357 WHERE cty_id = 117";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 564 WHERE cty_id = 118";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 491 WHERE cty_id = 119";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 451 WHERE cty_id = 120";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 334 WHERE cty_id = 122";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 308 WHERE cty_id = 124";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 194 WHERE cty_id = 125";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 471 WHERE cty_id = 127";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 211 WHERE cty_id = 129";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 277 WHERE cty_id = 130";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 446 WHERE cty_id = 131";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 464 WHERE cty_id = 132";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 391 WHERE cty_id = 133";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 654 WHERE cty_id = 134";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 445 WHERE cty_id = 135";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 579 WHERE cty_id = 136";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 522 WHERE cty_id = 137";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 391 WHERE cty_id = 138";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 467 WHERE cty_id = 140";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 299 WHERE cty_id = 141";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 741 WHERE cty_id = 143";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 438 WHERE cty_id = 144";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 502 WHERE cty_id = 145";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 290 WHERE cty_id = 147";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 444 WHERE cty_id = 148";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 294 WHERE cty_id = 151";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 378 WHERE cty_id = 152";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 501 WHERE cty_id = 155";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 352 WHERE cty_id = 156";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 459 WHERE cty_id = 159";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 351 WHERE cty_id = 160";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 420 WHERE cty_id = 161";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 391 WHERE cty_id = 162";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 505 WHERE cty_id = 164"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 370 WHERE cty_id = 165";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 706 WHERE cty_id = 166";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 625 WHERE cty_id = 167";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 490 WHERE cty_id = 168";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 447 WHERE cty_id = 169"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 654 WHERE cty_id = 170";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 338 WHERE cty_id = 171";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 675 WHERE cty_id = 172";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 480 WHERE cty_id = 173";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 565 WHERE cty_id = 175";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 391 WHERE cty_id = 178";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 433 WHERE cty_id = 180";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 459 WHERE cty_id = 181";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 567 WHERE cty_id = 184";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 421 WHERE cty_id = 182";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 202 WHERE cty_id = 185";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 437 WHERE cty_id = 186";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 927 WHERE cty_id = 190";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 326 WHERE cty_id = 191";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 531 WHERE cty_id = 202";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 601 WHERE cty_id = 203";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 526 WHERE cty_id = 204";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 407 WHERE cty_id = 205";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 334 WHERE cty_id = 206";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 465 WHERE cty_id = 207";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 195 WHERE cty_id = 209";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 492 WHERE cty_id = 210";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 235 WHERE cty_id = 211";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 470 WHERE cty_id = 213";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 288 WHERE cty_id = 214";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 451 WHERE cty_id = 217"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 467 WHERE cty_id = 218";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 251 WHERE cty_id = 219";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 491 WHERE cty_id = 220";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 530 WHERE cty_id = 232";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 463 WHERE cty_id = 233"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 322 WHERE cty_id = 221";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 683 WHERE cty_id = 236";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 654 WHERE cty_id = 223";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 297 WHERE cty_id = 237";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 316 WHERE cty_id = 238"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 421 WHERE cty_id = 239"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 519 WHERE cty_id = 240";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 221 WHERE cty_id = 241";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 654 WHERE cty_id = 242";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 284 WHERE cty_id = 243";

        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 401 WHERE cty_id = 244"; 
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 542 WHERE cty_id = 246";        
        $sql[] = "UPDATE tbl_shipping_countries SET ap_akg = 529 WHERE cty_id = 247";

        $deleteDR = "DELETE FROM WHERE cty_id = 61";        
        $deleteDR = "DELETE FROM WHERE cty_id = 65";        

        $pdo = $manager->getAdapterConnection('mpm');         
        
        foreach ($sql as $x) {
            $stmt = $pdo->prepare($x);
            $stmt->execute();
        }
            
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'mpm' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `tbl_shipping_countries`

  ADD `ap_fkg` DECIMAL AFTER `zone_id`,

  ADD `ap_akg` DECIMAL AFTER `ap_fkg`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'mpm' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `tbl_shipping_countries`

  DROP `ap_fkg`,

  DROP `ap_akg`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}