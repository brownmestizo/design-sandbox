<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1500969257.
 * Generated on 2017-07-25 07:54:17 by vagrant
 */
class PropelMigration_1500969257
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'mpm' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `tbl_menus`

  CHANGE `menu_alias` `menu_alias` TEXT NOT NULL;

ALTER TABLE `tbl_prod_info`

  CHANGE `prod_category` `prod_category` TEXT NOT NULL;

ALTER TABLE `tbl_prod_photos`

  CHANGE `prod_id` `prod_id` INTEGER NOT NULL;

ALTER TABLE `tbl_prod_prices`

  CHANGE `prod_id` `prod_id` INTEGER NOT NULL;

CREATE INDEX `tbl_prod_smaller_fi_04a582` ON `tbl_prod_smaller` (`sm_prod_price_id`);

ALTER TABLE `tbl_prod_smaller` ADD CONSTRAINT `tbl_prod_smaller_fk_f3ce8e`
    FOREIGN KEY (`prod_id`)
    REFERENCES `tbl_prod_info` (`prod_id`);

ALTER TABLE `tbl_prod_smaller` ADD CONSTRAINT `tbl_prod_smaller_fk_04a582`
    FOREIGN KEY (`sm_prod_price_id`)
    REFERENCES `tbl_prod_pricing` (`prod_price_id`);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'mpm' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `tbl_menus`

  CHANGE `menu_alias` `menu_alias` VARCHAR(5) NOT NULL;

ALTER TABLE `tbl_prod_info`

  CHANGE `prod_category` `prod_category` VARCHAR(5) NOT NULL;

ALTER TABLE `tbl_prod_photos`

  CHANGE `prod_id` `prod_id` INTEGER NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_prod_prices`

  CHANGE `prod_id` `prod_id` INTEGER NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_prod_smaller` DROP FOREIGN KEY `tbl_prod_smaller_fk_f3ce8e`;

ALTER TABLE `tbl_prod_smaller` DROP FOREIGN KEY `tbl_prod_smaller_fk_04a582`;

DROP INDEX `tbl_prod_smaller_fi_04a582` ON `tbl_prod_smaller`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}