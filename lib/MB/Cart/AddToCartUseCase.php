<?php

namespace MB\Cart;

class AddToCartUseCase
{
    /**
     * @var CartStorage
     */
    private $cs;
    /**
     * @var \TblProdInfo
     */
    private $product;
    /**
     * @var \TblStands
     */
    private $stand;
    private $quantity;
    private $size;
    private $websiteVars;

    public function __construct(
        CartStorage $cs, 
        \TblProdInfo $product, 
        \TblStands $stand, 
        $quantity, 
        $size,
        $websiteVars){        
            $this->cs = $cs;
            $this->product = $product;
            $this->stand = $stand;
            $this->quantity = $quantity;
            $this->size = $size;
            $this->websiteVars = $websiteVars;
    }

    public function execute()
    {
        if ( $this->product->getProdPriceId() > 0 )  
            $price = $this->product->getTblProdPricing()->getProdPricePrice();
        else 
            $price = $this->product->getTblProdPrices()->getProdNormalPrice();

        if ( is_null($this->product->getTblProdPhotosv2()) ) 
            $photo = $this->websiteVars['defaultImg'];
        else 
            $photo = $this->websiteVars['image_url'].$this->product->displayMainImage($this->websiteVars['website']);

        
        $weightEconomy = $this->product->getTblShippingCategories()->getWeightIde();
        $weight = \TblShippingEconomyQuery::create()->findOneByWeightIde($weightEconomy)->getWeightName();
        $product = new Product(
            $this->product->getProdId(),
            $this->product->getProdName(),
            $this->size == 2
                ? $this->product->getTblProdSmaller()->getTblProdPricing()->getProdPricePrice()
                : $price,
            $photo,
            $this->size,
            $weight,
            $weightEconomy,
            $this->product->getTblShippingCategories()->getWeightIdp(),
            [
                1 => $this->product->getTblShippingCategories()->getWeightAp1(),
                $this->product->getTblShippingCategories()->getWeightAp2(),
                $this->product->getTblShippingCategories()->getWeightAp3(),
                $this->product->getTblShippingCategories()->getWeightAp4(),
                $this->product->getTblShippingCategories()->getWeightAp5(),
                $this->product->getTblShippingCategories()->getWeightAp6(),
                $this->product->getTblShippingCategories()->getWeightAp7(),
                $this->product->getTblShippingCategories()->getWeightAp8(),
                $this->product->getTblShippingCategories()->getWeightAp9(),
                $this->product->getTblShippingCategories()->getWeightAp10(),
            ]
        );
        $stand = new Stand($this->stand->getStandId(), $this->stand->getStandName(), $this->stand->getStandPrice());
        $cart = $this->cs->load();
        $cart->addItem(new CartItem($product, $stand, $this->quantity));
        $this->cs->save($cart);
    }
}
