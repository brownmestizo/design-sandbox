<?php

namespace MB\Cart;

class Cart
{
    /** @var CartItem[] */
    private $items;
    /** @var array */
    private $details;

    public function __construct()
    {
        $this->items = [];
    }

    public function getItems()
    {
        return $this->items;
    }

    public function getItemsCount()
    {
        return count($this->items);
    }

    public function addItem(CartItem $cartItem)
    {
        foreach ($this->items as $item) {
            if ($cartItem->isSameAs($item)
            ) {
                $item->changeQty($item->getQuantity() + $cartItem->getQuantity());
                return;
            }
        }
        $this->items[] = $cartItem;
    }

    public function removeItem($id)
    {
        $this->items = array_filter($this->items, function (CartItem $item) use ($id) {
            return $item->getId() != $id;            
        });
    }

    public function changeItemQuantity($id, $qty)
    {
        foreach ($this->items as $item) {
            if ($item->getId() == $id) {
                $item->changeQty($qty);
                break;
            }
        }
    }

    public function price()
    {
        return array_reduce($this->items, function ($r, CartItem $ci) {
            return $r + $ci->totalPrice();
        }, 0);
    }

    public function totalWeight()
    {
        return array_reduce($this->items, function ($r, CartItem $ci) {
            return $r + $ci->totalWeight();
        }, 0.0);
    }

    public function setPaymentDetails($details)
    {
        $this->details = $details;
    }


    public function getPaymentDetails()
    {
        return $this->details;
    }
}
