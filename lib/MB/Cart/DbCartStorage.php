<?php

namespace MB\Cart;

use Symfony\Component\HttpFoundation\Session\Session;

class DbCartStorage implements CartStorage
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * @param Cart $cartToSave
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function save(Cart $cartToSave)
    {

        $id = $this->session->getId();
        $cart = \TblCartQuery::create()->findPk($id);
        if (!$cart) {
            $cart = new \TblCart();
            $cart->setId($id);
        }
        $cart->setDetails(\serialize($cartToSave));
        $cart->save();
    }

    public function saveWithNewId(Cart $cartToSave, $details)
    {
        $id = $this->newId();
        $cart = new \TblCart();
        $cart->setId($id);
        $cart->setDetails(\serialize($cartToSave));
        $cart->setPayment(\json_encode($details));
        $cart->save();

        return $id;
    }

    /**
     * @param null $id
     * @return Cart
     */
    public function load($id = null)
    {
        $id = $id ?: $this->session->getId();
        $cart = \TblCartQuery::create()->findPk($id);
        $return = $cart ? unserialize($cart->getDetails()) : new Cart();

        return $return;
    }

    private function newId()
    {
        return \bin2hex(\openssl_random_pseudo_bytes(8, $cs));
    }
}
