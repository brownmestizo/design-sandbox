<?php

namespace MB\Cart;


class Product
{
    const REGULAR_SIZE = 1;
    const SMALL_SIZE = 2;

    private $id;
    private $name;
    private $price;
    private $photo;
    private $size;
    private $weightE;
    private $weightP;
    private $weight;
    private $airWeights;

    public function __construct($id, $name, $price, $photo, $size, $weight, $weightE, $weightP, $airWeights)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->photo = $photo;
        $this->size = $size;
        $this->weightE = $weightE;
        $this->weightP = $weightP;
        $this->weight = $weight;
        $this->airWeights = $airWeights;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->size == self::SMALL_SIZE ? $this->name . ' Small Model' : $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @return mixed
     */
    public function getWeightEconomy()
    {
        return $this->weightE;
    }

    /**
     * @return mixed
     */
    public function getWeightPriority()
    {
        return $this->weightP;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getAirWeights()
    {
        return $this->airWeights;
    }
}
