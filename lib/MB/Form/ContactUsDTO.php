<?php

namespace MB\Form;

class ContactUsDTO
{
    public $firstName;
    public $emailAddress;
    public $message;
}
