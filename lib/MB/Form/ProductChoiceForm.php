<?php

namespace MB\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductChoiceForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $modelPrices = [$options['price'][1] => 1];

        //Smaller models
        if (isset($options['price'][2])) {
            $modelPrices[$options['price'][2]] = 2; 
        }

        $builder
            ->add('quantity', ChoiceType::class, [
                'label' => false,
                'choices' => array_combine(range(1, 6), range(1, 6)),
            ])
            ->add('product', ChoiceType::class, [
                'choices_as_values' => true,
                'choices' => $modelPrices,
                'label' => 'Model size',
            ])
            ->add('stand', ChoiceType::class, [
                'choices_as_values' => true,
                'choices' => $options['stands'],
                'label' => 'Stand (additional cost)',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => ProductChoiceDTO::class,
            ])
            ->setRequired(['stands', 'price']);
    }
}
