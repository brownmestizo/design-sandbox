<?php

namespace MB\Shipping;


class AirPricing
{
    private $first;
    private $above;
    private $phpToUsd;

    public function __construct($first, $above, $phpToUsd)
    {
        $this->first = $first;
        $this->above = $above;
        $this->phpToUsd = $phpToUsd;
    }

    public function calculateForWeight($weight)
    {
        if ($this->first == null || $this->above == null) {
            return null;
        }

        $totalWeight = max(1.0, $weight);
        $aboveKg = round($totalWeight - 1);

        return round(($this->first + $this->above * $aboveKg) / $this->phpToUsd, 2);
    }

}