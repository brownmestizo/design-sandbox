<?php

namespace MB\Shipping;


use MB\Cart\Cart;

class Shipping
{
    public $zone;
    public $economy;
    public $priority;
    private $priorityInfo;
    private $economyInfo;
    private $airPricing;

    public function __construct($countryId)
    {
        $country = \TblShippingCountriesQuery::create()->findOneByCtyId($countryId);
        if (!$country) {
            throw new \Exception('No country info found');
        }
        $zone = $country->getZoneId();
        $this->zone = \TblShippingZonesQuery::create()->findOneByZoneId($zone)->getZoneName();

        $this->economy = $country->getCtyEconomy() == 'Yes';
        $this->priority = $country->getCtyPriority() == 'Yes';
        $this->airPricing = new AirPricing($country->getAp_Fkg(), $country->getAp_Akg(), PHP_TO_USD);
        $this->fillEconomyInfo();
        $this->fillPriorityInfo();
    }

    public function getAirParcelPrice(Cart $cart)
    {
        $items = $cart->getItems();
        $price = 0;
        foreach ($items as $item) {
            $price += $this->airPricing->calculateForWeight(
                $item->getProduct()->getAirWeights()[$item->getQuantity()]
            );
        }

        return $price;
    }

    public function getPriorityPrice(Cart $cart)
    {
        if (!$this->priority) {
            return null;
        }

        $items = $cart->getItems();
        $price = 0;
        foreach ($items as $item) {
            $weightPriority = $item->getProduct()->getWeightPriority();
            $price += $this->priorityInfo[$this->priorityInfo['w'][$weightPriority] * $item->getQuantity()];
        }

        return $price;
    }

    public function getEconomyPrice(Cart $cart)
    {
        if (!$this->economy) {
            return null;
        }

        $items = $cart->getItems();
        $price = 0;

        foreach ($items as $item) {
            $weightEconomy = $item->getProduct()->getWeightEconomy();
            $price += $this->economyInfo[$this->economyInfo['w'][$weightEconomy] * $item->getQuantity()];
        }

        return $price;
    }

    public function getPrice($type, Cart $cart)
    {
        switch ($type) {
            case 'priority': return $this->getPriorityPrice($cart);
            case 'air': return $this->getAirParcelPrice($cart);
            case 'economy': return $this->getEconomyPrice($cart);
        }
    }

    public function getProductPrices(Cart $cart)
    {
        $items = $cart->getItems();
        $res = [];
        foreach ($items as $item) {
            $info = ['p' => null, 'e' => null, 'a' => $this->airPricing->calculateForWeight(
                $item->getProduct()->getAirWeights()[$item->getQuantity()]
            )];
            if ($this->economy) {
                $weightEconomy = $item->getProduct()->getWeightEconomy();
                $info['e'] = (float)$this->economyInfo[$this->economyInfo['w'][$weightEconomy] * $item->getQuantity()];
            }
            if ($this->priority) {
                $weightPriority = $item->getProduct()->getWeightPriority();
                $info['p'] = (float)$this->priorityInfo[$this->priorityInfo['w'][$weightPriority] * $item->getQuantity()];
            }

            $res[$item->getId()] = $info;
        }

        return $res;
    }

    private function fillEconomyInfo()
    {
        if (!$this->economy) {
            return null;
        }
        $info = \TblShippingEconomyQuery::create()->select(['weight_ide', 'weight_name', $this->zone])
            ->find()->toArray();
        $this->economyInfo = array_reduce($info, function ($r, $v) {
            $r[$v['weight_name']] = $v[$this->zone];
            $r['w'][$v['weight_ide']] = $v['weight_name'];

            return $r;
        }, []);
    }

    private function fillPriorityInfo()
    {
        if (!$this->priority) {
            return null;
        }
        $info = \TblShippingPriorityQuery::create()->select(['weight_idp', 'weight_name', $this->zone])
            ->find()->toArray();
        $this->priorityInfo = array_reduce($info, function ($r, $v) {
            $r[$v['weight_name']] = $v[$this->zone];
            $r['w'][$v['weight_idp']] = $v['weight_name'];

            return $r;
        }, []);
    }

}
