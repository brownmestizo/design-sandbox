<?php

namespace MB\Templating;

class CurrencyExtension extends \Twig_Extension
{
    private $currencies = [];

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('convert_rates', [$this, 'convertRates']),
            new \Twig_SimpleFunction('currencies', [$this, 'currenciesAvail']),
        ];
    }

    public function convertRates()
    {
        return \array_reduce($this->currencies(), function ($r, \TblCurrencyRates $currencyRates) {
            $r[$currencyRates->getCurrency()] = $currencyRates->getRate();

            return $r;
        }, ['USD' => 1]);
    }

    public function currenciesAvail()
    {
        return array_merge(['USD'], \array_map(function (\TblCurrencyRates $currencyRates) {
            return $currencyRates->getCurrency();
        }, $this->currencies()));
    }

    public function getName()
    {
        return 'currency';
    }

    private function currencies()
    {
        if (!$this->currencies) {
            $this->currencies = iterator_to_array(\TblCurrencyRatesQuery::create()->getIterator());
        }

        return $this->currencies ?: [];
    }
}
