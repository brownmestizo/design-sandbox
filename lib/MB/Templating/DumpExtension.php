<?php

namespace MB\Templating;

use MB\Cart\DbCartStorage;

class DumpExtension extends \Twig_Extension
{
    private $cs;

    public function __construct()
    {
        $this->cs = new DbCartStorage();
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('dump', 'var_dump'),
        ];
    }



    public function getName()
    {
        return 'dump';
    }
}
