<?php

namespace MB\Templating;


use Currency\CachingConverter;
use Currency\DbConverter;

class Templater
{
    public static function getTwig()
    {
        $filter_displayHtml = new \Twig_SimpleFilter('displayHtml', function ($string) {
            echo html_entity_decode($string, ENT_QUOTES, 'UTF-8');
        });

        $loader = new \Twig_Loader_Filesystem('views/');
        $twig = new \Twig_Environment($loader);
        $twig->addFilter($filter_displayHtml);
        $twig->addFilter(new \Twig_SimpleFilter('dump', function ($string) {
            var_dump($string);
        }));
        $twig->addExtension(new CartExtension());
        $twig->addExtension(new CurrencyExtension(new CachingConverter(new DbConverter())));
        $twig->addExtension(new DumpExtension());

        return $twig;
    }
}
