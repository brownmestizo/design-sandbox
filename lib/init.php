<?php

require_once '../vendor/autoload.php';

//Raden's Vagrantbox
$serverIPs[0] = '192.168.33.10';

//Hostgator
$serverIPs[1] = '192.185.69.136';

//Digital Ocean's
$serverIPs[2] = '128.199.154.181';

//Extract website folder name
$url = $_SERVER['REQUEST_URI'];

//Maximum results per page
$maxResultsPerPage = 20;
//$_SERVER['SERVER_ADDR'] ='127.0.0.1';
if (in_array($_SERVER['SERVER_ADDR'], ['192.185.69.136'])) {
    require '../config/config_hostgator.php';
} elseif (in_array($_SERVER['SERVER_ADDR'], ['127.0.0.1', '::1', '192.168.33.10'])) {
    require '../config/config_local.php';
} else {
    require '../config/config_online.php';
}

/**
 * @param $url
 * @return array
 */
function getWebsiteVars($url)
{
    $server = explode('/', $url);
    $variableToCheck = '';
    foreach ($server as $part) {
        if (in_array($part, ['modelbuffs.com', 'planearts.com', 'mymahoganymodel.com'])) {
            $variableToCheck = $part;
        }
    }

    $websiteVars = [];
    //Collation of website specific variables
    switch ($variableToCheck) {
        case 'modelbuffs.com' :
            if (in_array($_SERVER['SERVER_ADDR'], ['127.0.0.1', '::1', '192.168.33.10'])) {
                $productImagesURL = "https://www.modelbuffs.com/mpm/uploadsv2/";
            } else {
                $productImagesURL = '../../mpm/uploadsv2/';  
            }

            /*
            if (MODE) $productImagesURL = "https://www.modelbuffs.com/mpm/uploadsv2/";
            else $productImagesURL = '../../mpm/uploadsv2/';        
            */
            $websiteVars = [
                'website' => 'mb',
                'websiteURL' => 'modelbuffs.com',
                'websiteName' => 'Modelbuffs',
                'image_url' => $productImagesURL,
                'websiteSpecificImagePath' => '../assets/img/modelbuffs/',
                'defaultImg' => '../assets/img/default-modelbuffs.png',
                'css' => '../assets/css/style-mb.css',
                'sendInBlueSubID' => 1,
                'sendInBlueListID' => 2,
                'emailAddress' => "contact@modelbuffs.com", //from email
                'emailPassword' => "56pIh*T7&TXy", //from email password
                'emailServer' => "modelbuffs.com", //from email server
                'emailSMTPSPort' => "465", //from email smtpsport
                'contactEmailAddress' => "brownmestizo@gmail.com",
                'keywords' => 'Modelbuffs, Model Buffs, Factory, handmade museum quality, wood model airplanes, wooden helicopter models custom made scale handcrafted, hand painted wooden model aircraft, Made to Order, spacecraft, rockets, airships, airship, blimps, tail fins, boats, submarines, submarine, ships, ship, boat, military wall plaques , seals, solid kiln dried Philippine mahogany, British authentic aircraft, Private, Civilian, Military Jet, Propeller, Glider, Science Fiction, Space NASA Experimental, nose art aviation, collectibles, executive Gifts, promotional incentives, hand carved, Pacific, handpainted, logo, carved, stand, authentic, personalized, direct, helicopters, replica, bespoke, blueprints, Clark Air Base, Warplane, Warplanes, bravo delta',
                'description' => 'ModelBuffs is your one stop factory source for handmade museum quality wood model airplanes and wooden helicopter models.  Stock, bespoke and custom made personalised scale handcrafted and hand painted wooden model aircraft, helicopters, spacecraft, rockets, airships and blimps, tail fins, boats, submarines  and ships, military wall plaques and seals all made out of solid kiln dried, renewable Philippine mahogany',
            ];
            break;

        case 'planearts.com' :
            if (in_array($_SERVER['SERVER_ADDR'], ['127.0.0.1', '::1', '192.168.33.10'])) {
                $productImagesURL = "https://www.modelbuffs.com/mpm/uploadspav2/";
            } else {
                $productImagesURL = '../../mpm/uploadspav2/';  
            }        
            /*
            if (MODE) $productImagesURL = "https://www.modelbuffs.com/mpm/uploadspav2/";
            else $productImagesURL = '../../mpm/uploadspav2/';      
            */

            $websiteVars = [
                'website' => 'pa',
                'websiteURL' => 'planearts.com',
                'websiteName' => 'Planearts',
                'image_url' => $productImagesURL,
                'websiteSpecificImagePath' => '../assets/img/planearts/',
                'defaultImg' => '../assets/img/default-planearts.png',
                'css' => '../assets/css/style-pa.css',
                'sendInBlueSubID' => 2,
                'sendInBlueListID' => 4,
                'emailAddress' => 'sales@planearts.com',
                'emailPassword' => 'oddywalker',
                'emailServer' => 'mail.planearts.com',
                'emailSMTPSPort' => '465',
                'contactEmailAddress' => 'sales@planearts.com',
                'keywords' => 'Planeart, Planearts, manufacturers, museum quality hand made model airplanes, warplane, warplanes, model helicopters, helicopter, airships, airship, blimps, dirigibles, tail Shields, wall plaques, seals, boats ships, custom Made wooden collectible models direct from factory, Clark Air Base, Philippines, marketing, exactness, detail, birthday, Christmas gifts, presentations, collector\'s, desktops, showcases, museums, flying Schools, reception areas, offices, aviation, hand carved hand painted, plastic, resin, die cast, truly wood, generations, heirlooms, your own or  favourite Airplane livery, base, brass, plaque, foam, FedEx,Pacific, Customized, customised, handpainted',
                'description' => 'Production facilities near the former Clark Air Base in the Philippines, Planearts manufacture and sell wood model airplanes direct from the factory.  The exactness and detail of your model is guaranteed from start to finish.  Ideal for birthday or Christmas gifts, presentations, collector\'s items, desktops, showcases, museums, flying schools, reception areas, offices and many aviation or other related location',
            ];
            break;

        case 'mymahoganymodel.com' :
            if (in_array($_SERVER['SERVER_ADDR'], ['127.0.0.1', '::1', '192.168.33.10'])) {
                $productImagesURL = "https://www.modelbuffs.com/mpm/uploadsm3v2/";
            } else {
                $productImagesURL = '../../mpm/uploadsm3v2/';  
            }            
            /*
            if (MODE) $productImagesURL = "https://www.modelbuffs.com/mpm/uploadsm3v2/";
            else $productImagesURL = '../../mpm/uploadsm3v2/';      
            */

            $websiteVars = [
                'website' => 'm3',
                'websiteURL' => 'mymahoganymodel.com',
                'websiteName' => 'MyMahoganyModels',
                'image_url' => $productImagesURL,
                'websiteSpecificImagePath' => '../assets/img/mymahoganymodel/',
                'defaultImg' => '../assets/img/default-mymahoganymodel.png',
                'css' => '../assets/css/style-m3.css',
                'sendInBlueSubID' => 3,
                'sendInBlueListID' => 5,
                'emailAddress' => 'sales@mymahoganymodel.com',
                'emailPassword' => 'oddywalker',
                'emailServer' => 'mail.mymahoganymodel.com',
                'emailSMTPSPort' => '465',
                'contactEmailAddress' => 'sales@mymahoganymodel.com',
                'keywords' => 'bravo, delta, MyMahoganyModel, desktop made hand airplanes,  wood model, wooden helicopter models, wooden toy planes, custom made personalised scale handcrafted hand painted aircraft, helicopters, spacecraft, rockets, airships, airship, blimps, tail shields, boats, boat, ship, submarines, submarine, ships, military wall plaques, seals, kiln dried renewable Philippine mahogany, hardwood, UK, company production, Philippines, museum quality wooden model aircraft, Private, Civilian, Military Jet Warplanes, Propeller, Glider, Science Fiction, Space NASA, nose art panels, replica collectibles, executive gifts, promotional incentives, exhibits, handpainted Replicate, Pacific warplane,  Clark Air Base, FedEx, blueprints',
                'description' => 'MyMahoganyModel offer stock( and custom made) authentic museum quality wooden model aircraft in Private & Civilian, Military Jet Warplanes, Military Propeller, Glider,  Science Fiction, Space, NASA and Experimental  categories - even  aviation nose art panels',                
            ];
            break;
    }

    return $websiteVars;
}

$websiteVars = getWebsiteVars($url);
