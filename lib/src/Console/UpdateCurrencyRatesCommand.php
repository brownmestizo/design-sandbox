<?php

namespace Console;

use Currency\Converter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCurrencyRatesCommand extends Command
{
    /**
     * @var Converter
     */
    private $converter;

    /**
     * @param Converter $converter
     */
    public function __construct(Converter $converter)
    {
        parent::__construct();
        $this->converter = $converter;
    }

    protected function configure()
    {
        $this->setName('currency:update')
            ->setDescription('Updates currency rates')
            ->addOption('currencies', null, InputOption::VALUE_OPTIONAL, 'Comma-separated list of currencies')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currencies = ['AUD','CAD', 'DKK', 'EUR', 'HKD', 'JPY', 'NOK', 'GBP', 'SGD', 'SEK', 'CHF',];
        if ($currenciesStr = $input->getOption('currencies')) {
            $currencies = \array_map('strtoupper', \explode(',', $currenciesStr));
        }

        foreach ($currencies as $currency) {
            $rate = $this->converter->getRate($currency);
            $this->saveRate($rate, $currency);
        }
    }

    private function saveRate($rate, $currency)
    {
        if (!$rate) {
            return;
        }
        $currencyRate = \TblCurrencyRatesQuery::create()->findOneByCurrency($currency);
        if (!$currencyRate) {
            $currencyRate = new \TblCurrencyRates();
            $currencyRate->setCurrency($currency);
        }
        $currencyRate->setRate($rate);
        $currencyRate->save();
    }
}
