<?php

namespace Currency;

abstract class AbstractConverter implements Converter
{
    /**
     * @param $value
     * @param $currency
     * @return mixed
     */
    public function convertFromUsd($value, $currency)
    {
        return round($value * $this->getRate($currency), 2);
    }
}
