<?php

namespace Currency;

use JOJO_Currency_yahoo;

class ApiConverter extends AbstractConverter
{
    /**
     * @var JOJO_Currency_yahoo
     */
    private $api;

    public function __construct()
    {
        require_once __DIR__ . '/../../currency/currencyConverter.php';
        $this->api = new JOJO_Currency_yahoo();
    }

    /**
     * @param $currency
     * @return mixed
     */
    public function getRate($currency)
    {
        return $this->api->getRate('USD',$currency, true);
    }
}
