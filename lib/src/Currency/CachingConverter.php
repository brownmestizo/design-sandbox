<?php

namespace Currency;

class CachingConverter extends AbstractConverter
{
    /**
     * @var Converter
     */
    private $converter;
    private $rates = [];

    public function __construct(Converter $converter)
    {
        $this->converter = $converter;
    }

    /**
     * @param $currency
     * @return mixed
     */
    public function getRate($currency)
    {
        if (\array_key_exists($this->rates, $currency)) {
            return $this->rates[$currency];
        }

        $rate = $this->converter->getRate($currency);
        if ($rate !== null) {
            $this->rates[$currency] = $rate;
        }

        return $rate;
    }
}
