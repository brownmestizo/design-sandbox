<?php

namespace Currency;


interface Converter
{
    /**
     * @param $currency
     * @return mixed
     */
    public function getRate($currency);

    /**
     * @param $value
     * @param $currency
     * @return mixed
     */
    public function convertFromUsd($value, $currency);
}
