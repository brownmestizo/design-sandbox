<?php

namespace Currency;

class DbConverter extends AbstractConverter
{
    /**
     * @param $currency
     * @return mixed
     */
    public function getRate($currency)
    {
        $currencyObj = \TblCurrencyRatesQuery::create()->findOneByCurrency($currency);
        if (!$currencyObj) {
            return null;
        }

        return $currencyObj->getRate();
    }
}
