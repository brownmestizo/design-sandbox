<?php

namespace Mail;

class Mailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * Mailer constructor.
     * @param \Swift_Transport $transport
     */
    public function __construct(\Swift_Transport $transport)
    {
        $this->mailer = \Swift_Mailer::newInstance($transport);
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $subj
     * @param string $body
     * @param array  $bcc
     */
    public function send($from, $to, $subj, $body, $bcc = [])
    {
        $message = \Swift_Message::newInstance($subj, $body);
        $message->setFrom($from);
        $message->setTo($to);
        $message->setBcc($bcc);
        $message->setContentType('text/html');

        $this->mailer->send($message);
    }
}
