<?php

final class PkList
{
    private $pks = [];

    private function __construct($arr)
    {
        $this->pks = $arr;
    }

    /**
     * @param $string
     * @return PkList
     */
    public static function fromString($string)
    {
        return self::fromArray(explode(' ', $string));
    }

    /**
     * @param $arr
     * @return PkList
     */
    public static function fromArray($arr)
    {
        $list = new self(array_map(function ($x) {
            return (intval($x));
        }, $arr));

        return $list;
    }

    /**
     * @return string
     */
    public function toString()
    {
        return implode(' ', $this->pks);
    }

    /**
     * @return array
     */
    public function pks()
    {
        return $this->pks;
    }
}
