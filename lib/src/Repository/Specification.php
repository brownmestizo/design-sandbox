<?php

namespace Repository;

use Propel\Runtime\ActiveQuery\ModelCriteria;

interface Specification
{
    /**
     * @param ModelCriteria $criteria
     *
     * @return string
     */
    public function apply(ModelCriteria $criteria);
}
