<?php

namespace Repository;

use Propel\Runtime\ActiveQuery\ModelCriteria;

class SpecificationRepository
{
    /**
     * @var ModelCriteria
     */
    private $criteria;

    /**
     * @param ModelCriteria $criteria
     */
    public function __construct(ModelCriteria $criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * @param Specification $specification
     *
     * @return ModelCriteria
     */
    public function findBySpec(Specification $specification)
    {
        $name = $specification->apply($this->criteria);
        if ($name) {
            $this->criteria->where([$name], ModelCriteria::LOGICAL_OR);
        }

        return $this->criteria;
    }
}
