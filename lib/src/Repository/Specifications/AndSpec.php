<?php

namespace Repository\Specifications;

use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class AndSpec extends Specification
{
    /**
     * @var
     */
    private $specs;

    public function __construct($specs = [])
    {
        \array_map([$this, 'addSpec'], $specs);
    }

    /**
     * @param ModelCriteria $criteria
     * @return string
     */
    public function apply(ModelCriteria $criteria)
    {
        if (!$this->specs) {
            return null;
        }

        $criterias = array_filter(\array_map(function (Specification $spec) use ($criteria) {
            return $spec->apply($criteria);
        }, $this->specs));

        if (count($criterias) === 1) {
            return current($criterias);
        }

        $name = $this->generateName();
        $criteria->combine($criterias, Criteria::LOGICAL_AND, $name);

        return $name;
    }

    /**
     * @param Specification $specification
     */
    public function addSpec(Specification $specification)
    {
        $this->specs[] = $specification;
    }
}
