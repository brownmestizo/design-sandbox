<?php

namespace Repository\Specifications\Product;

use Map\TblProdInfoTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Repository\Specifications\Specification;

class CategoriesSpec extends Specification
{
    private $categories;

    /**
     * @param array $categories
     */
    public function __construct($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param ModelCriteria $criteria
     *
     * @return string
     */
    public function apply(ModelCriteria $criteria)
    {
        if (!$this->categories) {
            return null;
        }
        $name = $this->doApply(
            $criteria,
            \sprintf('%s IN ?', TblProdInfoTableMap::COL_PROD_CATEGORY),
            $this->categories
        );

        return $name;
    }
}
