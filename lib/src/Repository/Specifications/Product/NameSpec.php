<?php

namespace Repository\Specifications\Product;

use Propel\Runtime\ActiveQuery\ModelCriteria;
use Repository\Specifications\Specification;

class NameSpec extends Specification
{
    /**
     * @var
     */
    private $terms;

    public function __construct($terms)
    {
        $this->terms = $terms;
    }

    /**
     * @param ModelCriteria $criteria
     *
     * @return string
     */
    public function apply(ModelCriteria $criteria)
    {
        if (!$this->terms) {
            return null;
        }

        foreach ($this->terms as $term) {
            $conditionName = $this->generateName();
            $conditionNames[] = $conditionName;
            $criteria->condition($conditionName,
                'LOWER(tbl_prod_info.prod_name) LIKE ?', '%' . strtolower($term) . '%');
        }

        if (count($conditionNames) === 1) {
            return \current($conditionNames);
        }

        $name = $this->generateName();
        $criteria->combine($conditionNames, ModelCriteria::LOGICAL_AND, $name);

        return $name;
    }
}
