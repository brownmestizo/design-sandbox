<?php

namespace Repository\Specifications\Product;

use Propel\Runtime\ActiveQuery\ModelCriteria;
use Repository\Specifications\Specification;

class WebsiteSpec extends Specification
{
    /**
     * @var
     */
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param ModelCriteria $criteria
     *
     * @return string
     */
    public function apply(ModelCriteria $criteria)
    {
        return $this->doApply($criteria, \sprintf('TblProdInfo.%s = ?', $this->value), 1);
    }
}
