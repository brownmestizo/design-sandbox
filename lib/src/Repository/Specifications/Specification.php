<?php

namespace Repository\Specifications;

use Propel\Runtime\ActiveQuery\ModelCriteria;
use Repository\Specification as SpecificationInterface;

abstract class Specification implements SpecificationInterface
{
    /**
     * @param ModelCriteria $criteria
     * @param string        $cond
     * @param mixed         $value
     * @return string
     */
    protected function doApply(ModelCriteria $criteria, $cond, $value)
    {
        $rand = $this->generateName();
        $criteria->condition($rand, $cond, $value);

        return $rand;
    }

    /**
     * @return string
     */
    protected function generateName()
    {
        $rand = \bin2hex(\openssl_random_pseudo_bytes(10));

        return $rand;
    }
}
