<?php
use Form\FormBuilder;
use Mail\Mailer;
use MB\Form\ContactsForm;
use MB\Templating\Templater;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$twig = Templater::getTwig();
$builder = new FormBuilder($twig);
$contacts = new \MB\Form\ContactUsDTO();
$form = $builder->getForm(ContactsForm::class, $contacts, ['method' => 'POST']);
$request = Request::createFromGlobals();

$form->handleRequest($request);

$congrats = 0;
if ($form->isValid()) {
    $mailer = new Mailer((new Swift_SmtpTransport(
        $websiteVars['emailServer'],
        $websiteVars['emailSMTPSPort'],
        'ssl'))->setUsername($websiteVars['emailAddress'])
        ->setPassword($websiteVars['emailPassword'])
    );
    $mailer->send(
        $websiteVars['emailAddress'],
        $websiteVars['contactEmailAddress'],
        $websiteVars['websiteName'].' - Website message from '.$contacts->firstName,
        sprintf(
            'Name: %s<br/>Address: %s<br/>Message:<br/> %s',
            $contacts->firstName,
            $contacts->emailAddress,
            $contacts->message
            )
        );

    $congrats = 1;
}

$form = $form->createView();


echo $twig->render('page_contactus.html', compact('websiteVars', 'congrats', 'form'));
