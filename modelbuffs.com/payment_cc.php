<?php

use MB\Cart\Cart;
use MB\Cart\DbCartStorage;
use MB\Shipping\Shipping;
use MB\Templating\Templater;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

require_once '../lib/init.php';

$countries = TblShippingCountriesQuery::create()->select(array('cty_id', 'cty_name'))->find()->toArray();

$twig = Templater::getTwig();
$cs = new DbCartStorage();
$cart = $cs->load();

require_once '../lib/Stripe.php';

$error = null;
$errorMessage = null;
$success = null;
$request = Request::createFromGlobals();
$shipmentType = $request->get('type', null);
$shipmentCty = $request->get('cty', null);

$shipmentCountry = TblShippingCountriesQuery::create()->findPk($shipmentCty);

if (!in_array($shipmentType, ['priority', 'economy', 'air'])) {
    header('Location: shipping.php');
    exit;
}

try {
    $shipping = new Shipping($shipmentCty);
} catch (\Exception $e) {
    header('Location: shipping.php');
    exit;
}


$shipping = $shipping->getPrice($shipmentType, $cart);
$amount = $cart->price() + $shipping;



if ($request->isMethod('post')) {

    Stripe::setApiKey('sk_live_tBV9CUjPr0a0VxOBAwPWObcM');
    try {
        if (empty($_POST['street']) || empty($_POST['city']) || empty($_POST['zip'])) {
            throw new Exception("Fill out all required fields.");
        }

        if (!isset($_POST['stripeToken'])) {
            throw new Exception("The Stripe Token was not generated correctly");
        }

        $id = $cs->saveWithNewId($cart, array_merge($_POST, compact('shipping')));
        unset($_POST['cardnumber']);
        $charge = Stripe_Charge::create([
            "amount" => intval($amount * 100),
            "currency" => "usd",
            "card" => $_POST['stripeToken'],
            "metadata" => [
                'cart' => $id,
                'billing-details' => json_encode($_POST),
                ],
            "description" => $websiteVars['websiteName']." - ".$_POST['email'],
        ]);


 
        
        if (!$charge) {
            throw new Exception('Can\'t charge the card');
        }
        if ($charge->failure_message) {
            throw new Exception($charge->failure_message);
        }
        $cs->save(new Cart());


        $mailer = new \Mail\Mailer(Swift_SmtpTransport::newInstance(
            $websiteVars['emailServer'],
            $websiteVars['emailSMTPSPort'],
            'ssl'
        )
            ->setUsername($websiteVars['emailAddress'])
            ->setPassword($websiteVars['emailPassword'])
        );

        $p = $_POST;

        $mailer->send(
            $websiteVars['emailAddress'],
            $_POST['email'],
            'Purchase details',
            $twig->render('customer_cc.html', compact('cart', 'p', 'amount', 'shipping', 'websiteVars', 'id')),
            [$websiteVars['contactEmailAddress']]
        );

        $mailer->send(
            $websiteVars['emailAddress'],
            $websiteVars['contactEmailAddress'],
            'Order from '.$_POST['firstName']." ".$_POST['lastName'],
            $twig->render('merchant_email.html', compact('cart', 'p', 'amount', 'shipping', 'websiteVars'))
        );      
        
        
        ob_end_clean();
        header("Connection: close");
        header('Location: congratulation.php?mt=cc&id=' . $id);

        ignore_user_abort(); // optional
        ob_start();
        echo json_encode(['ok' => $id]);
        $size = ob_get_length();
        header("Content-Length: $size");
        ob_end_flush();
        flush();
        session_write_close();
        
  

        exit;
    } catch (Exception $e) {
        $errorMessage = $e->getMessage();
        $error = true;
    }
}

// For valid CCs for testing, please visit https://stripe.com/docs/testing
$finalStep = true;


echo $twig->render(
    'page_payment_cc.html',
    compact('cart', 'generated_image_url', 'websiteVars', 'error', 'errorMessage', 
        'countries', 'success', 'amount', 'shipmentCountry', 'shipping', 'finalStep'));