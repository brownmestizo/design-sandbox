<?php
use MB\Templating\Templater;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$twig = Templater::getTwig();

// Variables
$request = Request::createFromGlobals();
$pageNumber = $request->get('page', 1);
$searchTerm = $request->get('q', '');


$categories = TblMenusQuery::create()
    ->find();

$latestProductID = TblProdInfoQuery::create()
    ->orderByProdId('desc')
    ->limit(1)
    ->findOne();

$conditionNameTpl = 'searchCond%d';
$searchTerms = explode(' ', $searchTerm);
$conditionNames = [];
$productsQuery = TblProdInfoQuery::create();
foreach ($searchTerms as $i => $term) {
    $conditionName = sprintf($conditionNameTpl, $i);
    $conditionNames[] = $conditionName;
    $productsQuery->condition($conditionName,
        'LOWER(tbl_prod_info.prod_name) LIKE ?', '%' . strtolower($term) . '%');
}

$productsPager = $productsQuery->useTblProdPhotosQuery()
        ->filterByProdId(['max' => $latestProductID->getProdId()])
    ->endUse()
    ->where('TblProdInfo.' . $websiteVars['website'] . ' = ?', 1)
    // Criteria::LOGICAL_OR another option here
    ->where($conditionNames, Criteria::LOGICAL_AND)
    ->orderByProdId('desc')
    ->paginate($pageNumber, $maxResultsPerPage);

$page = 'search';
$q = $searchTerm;
$params = compact('q', 'page');    

echo $twig->render(
    'page_search.html',
    compact('productsPager', 'categories', 'pageNumber', 'websiteVars', 'searchTerm', 'params')
);


//
//echo $twig->render(
//    'page_search.html',
//    [
//        'newProducts' => $newProducts,
//        'websiteVars' => $websiteVars,
//    ]);
