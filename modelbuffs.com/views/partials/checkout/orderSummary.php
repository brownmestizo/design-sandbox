<div class="contentBox">
    <div class="w100 costDetails">
        <div class="table-block" id="order-detail-content">

            <div class="w100 cartMiniTable">
                <table id="cart-summary" class="std table">
                    <tbody>
                    <tr>
                        <td colspan="2">ORDER SUMMARY</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="row p-t-sm">
                                <div class="col-xs-8">
                                    Estimated total                            
                                </div>
                                <div class="col-xs-4 text-right">
                                    <span class="text-right site-color total-price js-cart-price" id="total-price" data-price="{{ cart.price }}">
                                        ${{ cart.price }}
                                    </span>                            
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Shipping</td>
                        <td class="site-color js-total-shipping">{{ shipping }}</td>
                    </tr>                    
                    </tbody>
                </table>

                <table class="std table no-border m-b-none">
                    <tbody>
                    <tr>
                        <td><strong>Your total price</strong></td>
                        <td class="site-color text-right">
                            <strong class="js-summary">${{ cart.price + shipping }}</strong>
                        </td>
                    </tr>
                    </tbody>
                </table>                

                {% if not finalStep %}                 
                <div class="p-xxs">
                    <p><i class="fa fa-info-circle"></i> Shipping costs will be applied during checkout</p>
                </div>
                {% endif %}

            </div>

            {% if shippingStatus %}
            <a class="btn btn-primary btn-lg btn-block m-t-sm" title="checkout" href="shipping.php">
                COMPUTE SHIPPING
            </a>
            {% endif %}

            {% if paymentStatus %}
            <a class="btn btn-primary btn-lg btn-block m-t-sm js-go-to-payment" title="checkout" href="payment.php">
                PAY NOW
            </a>         
            {% endif %}

            {% if amount %}
                <!-- Important notice -->
                <div class="form-group m-t-lg">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Important notice</h3>
                        </div>
                        <div class="panel-body">
                            <p>Your card will be charged <strong>USD${{amount}}</strong> only upon payment confirmation.</p>
                            <p>Your account statement will show the following transaction description - {{websiteVars.websiteName}} Model Plane Purchase.</p>
                        </div>
                    </div>           
                </div>
            {% endif %}

        </div>
    </div>
</div>
