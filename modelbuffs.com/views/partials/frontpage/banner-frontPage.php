<div class="parallax-section parallax-image-1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="homepage-banner parallax-content clearfix">

                    <div class="row first-row">
                        <div class="col-lg-7 col-1">
                        </div>

                        <div class="col-lg-5 col-2">
                            <h1 class="m-b-sm">Find your aircraft</h1>
                            <h3 class="m-b-none">If you've seen it somewhere, we most likely have a model of it.</h3>
                            <form id="search-form" method="GET" action="search.php" role="search" class="navbar-form">
                                <div class="clearfix">
                                    <input type="text" name="q" value="{{ searchTerm|default('') }}" placeholder="Search using a keyword or keyphrase" class="form-control">
                                </div>

                                <div class="clearfix text-center">
                                    <h4 style="margin-bottom: 10px;">OR</h4>
                                </div>

                                <div class="input-append newsLatterBox text-center">
                                    <a class="btn bg-gray" href="category.php?category=13">
                                        Browse all products <i class="fa fa-long-arrow-right"> </i>
                                    </a>
                                </div>                                                                    

                            </form>

                        </div>
                    </div>

                    <!--
                    <div class="row second-row">
                        <div class="col-lg-7 col col-1">
                            <div>
                                <h3 class="m-b-none">LARGE SCALE MODELS</h3>
                                <p>Browse a wide range of models</p>

                                <a class="btn btn-primary m-t-sm m-b-xxs" href="category.php?category=13">
                                    Browse all products <i class="fa fa-long-arrow-right"> </i>
                                </a>                                

                            </div>

                        </div>

                        <div class="col-lg-5 col col-2">
                            <div>
                                <h3 class="m-b-none">Custom made model?</h3>
                                <p>Can't find it? We can build it.</p>
                            </div>
                        </div>
                    </div>
                    -->

                </div>
            </div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!--/.parallax-image-1-->


<div id="feature1" class="feature1-section">
  <div class="container">
    <div class="feature-1-row row">
      <div class="feature-1-left-col col-sm-6">
        <h2>{{websiteVars.websiteName}} is a one stop factory source for handmade museum quality wood model airplanes</h2>

        <div class="feature-media-row row">

          <div class="feature-media-col col-md-6">
            <div class="feature-media media">
              <div class="media-body">
                <h4 class="media-heading">British owned company</h4>
                <p>With manufacturing facilities in the Philippines, we offer custom made and standard "off the shelf" authentic quality wooden model aircraft</p>
              </div>
            </div>
          </div>

          <div class="feature-media-col col-md-6">
            <div class="feature-media media">
              <div class="media-body">
                <h4 class="media-heading">Straight from the factory</h4>
                <p>Unlike our competitors, we run our business direct from the factory source and not through third party companies or regional trading offices</p>
              </div>
            </div>
          </div>

          <div class="feature-media-col col-md-6">
            <div class="feature-media media">
              <div class="media-body">
                <h4 class="media-heading">Authentic detail</h4>
                <p>These models are packed with authentic detail based on the actual aircraft blueprints</p>
              </div>
            </div>
          </div>

          <div class="feature-media-col col-md-6">
            <div class="feature-media media">
              <div class="media-body">
                <h4 class="media-heading">Handcrafted models</h4>
                <p>All the models you see on the Modelbuffs website are handcrafted and hand painted by our gifted artists and master craftsmen</p>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- .feature-1-left-col -->

      <div class="feature-1-right-col col-sm-5 col-sm-offset-1">
        <div class="double-screen-right">
          <img class="screen top" src="../assets/img/mb-feature.png" alt="">
        </div>
      </div> <!-- .feature-1-right-col -->
    </div>
  </div>
</div>

<div class="container m-t-lg">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">        

            <p class="lead">{{websiteVars.websiteName}} is a one stop factory source for handmade museum quality wood model airplanes and wooden helicopter models. Stock, bespoke and custom made personalised scale handcrafted and hand painted wooden model aircraft, helicopters, spacecraft, rockets, airships and blimps, tail fins, boats, submarines  and ships, military wall plaques and seals all made out of solid kiln dried, renewable Philippine mahogany (commonly known as Lauan or Meranti).</p>

            <p>As a British owned company with manufacturing facilities in the Philippines, {{websiteVars.websiteName}}.com® offers custom made and standard "off the shelf" authentic quality wooden model aircraft in Private & Civilian, Military / Warplane Jet, Military Propeller Warplanes, Glider, Airship, Submarine, Ship, Boat, Science Fiction, Space, NASA and Experimental  categories - even  aviation "nose art" panels.  Unlike our competitors, {{websiteVars.websiteName}}.com® run their business direct from the factory source and not through third party companies or regional trading offices.  The quality of your model replica is assured from day one of manufacture.</p>



        </div>

        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <p>Our reputable model airplanes are ideally suited as collectibles, executive gifts, premium and promotional incentives, art gallery sculptures, and many other purposes.  Each model is hand carved and completely handpainted to match the aircrafts livery, paint scheme and registration number.  Any logo can be carved or painted on the stand and a brass plaque with your personal inscription adds that extra finishing touch to your model. Please visit our Testimonial Page to see the kind words that our customers have had to say about our models.</p>

            <p>All the models you see on the {{websiteVars.websiteName}} website  are handcrafted and hand painted by our gifted artists and master craftsmen, and are packed with authentic detail based on the actual aircraft blueprints. The photographs you see have not been manipulated or edited - they are exactly as the model looks in real life but unfortunately even digital photography cannot do full justice to the look and feel of the model!</p>
        </div>
    </div>
</div>

<div class="morePost featuredPostContainer style2 globalPaddingTop ">
    <h3 class="section-title style2 text-center"><span>MADE TO ORDER MODELS</span></h3>

    <div class="container">   
        <div class="row xsResponse">
            <p>Most models are made to order and take about 8-10 weeks* to manufacture (even customised or personalised models) which is significantly quicker than the vast majority of our competitors. You will also notice that we offer models at prices way below other companies advertising on the web. We are able do this because of the direct manufacturing and selling basis of our business - models direct from the factory located near Clark Air Base in the Philippines, shipped through FedEx or DHL International Economy Courier service and delivered to your home across the Pacific or the Atlantic within 5 business days from dispatch. The price of each model is inclusive of the stand of your choice and full export packing. All prices are in US Dollars. * from date of order confirmation</p>
            <p>If you do not find your required model in our database or if you have any requirements that are not covered in the "Made to Order" section of our site then please email us directly at {{websiteVars.websiteName}}. We can make a model of any airplane or helicopter that you require – in any scale and in any size or configuration and with any markings or livery.</p>
            <p>Working from your photographs and our library of aircraft blueprints, our master craftsmen will meticulously recreate your airplane into an amazingly detailed desktop replica. Each model is hand carved from solid mahogany and completely hand painted to match your aircraft paint scheme and registration number. No detail will be spared!</p>
        </div>

    </div>

</div>
    