<link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">    
<link href="../assets/bootstrap-formhelper/dist/css/bootstrap-formhelpers.min.css" rel="stylesheet" media="screen">
<link href="../assets/plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" media="screen">    
<link href="../assets/plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet" media="screen">        
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700|Montserrat:700' rel='stylesheet' type='text/css'>    
<link href="{{websiteVars.css}}" rel="stylesheet">
<!-- Just for debugging purposes. -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- include pace script for automatic web page progress bar  -->

<script>
    paceOptions = {
        elements: true
    };
</script>
<script src="../assets/js/pace.min.js"></script>