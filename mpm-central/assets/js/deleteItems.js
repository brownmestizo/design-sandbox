var itemsToDelete = [],
    nameList = $('.js-name-list'),
    deleteButton = $('.js-delete-button'),
    confirmDelete = $('.js-confirm-delete');
;

$('.js-delete-one').click(function (e) {
    itemsToDelete = [{
        name: this.getAttribute('data-name'),
        id: this.getAttribute('data-id'),
    }];
    populatePopup();
});

function toggleDeleteButton() {
    if ($('.js-show-delete:checked').length > 0) {
        deleteButton.show();
    } else {
        deleteButton.hide();
    }
}

confirmDelete.click(function (e) {
    var ids = [];
    var type = this.getAttribute('data-name');

    itemsToDelete.forEach(function (x) {
        ids.push(x.id);
    });

    var string = ids.join(',');
    location.href = 'delete.php?ids='+string+'&type='+type;
});

deleteButton.click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    itemsToDelete = [];
    $('.js-show-delete:checked').each(function() {
        itemsToDelete.push({
            name: this.getAttribute('data-name'),
            id: this.getAttribute('data-id'),
        });
    });
    populatePopup();
    $('#deleteModal').modal('show');
});

toggleDeleteButton();

$('.js-show-delete').click(toggleDeleteButton);

function populatePopup() {
    nameList.html('');
    itemsToDelete.forEach(function (x) {
        nameList.append('<li>' + x.name + '</li>');
    });
}