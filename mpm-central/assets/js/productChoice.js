// Better rewrite to use promises, but don't really know browser version requirements
var relatedProducts = [];
function setUpSearch(elem, cb) {
    elem.addEventListener('keyup', function () {
        fetchRelatedByStr(this.value);
    });
    function fetchRelatedByStr($str) {
        if ($str.length < 2) {
            return
        }
        $.get('../api/index.php/products/search/' + $str + '?not=' + relatedIdsAsString(), function (data) {
            if (data && data.TblProdInfos) {
                cb(data.TblProdInfos);
            }
        }, 'json');
    }
}
function buildProductLine(d, onClick, type) {
    var div = document.createElement('div');
    div.className = 'i-checks m-b-xs';
    var label = document.createElement('label');
    div.appendChild(label);
    var input = document.createElement('input');
    input.type = type || 'checkbox';
    input.name = 'someinputname';
    input.setAttribute('data-id', d.prod_id);
    label.setAttribute('data-id', d.prod_id);
    label.appendChild(input);
    label.appendChild(document.createTextNode(' ' + d.prod_name));
    label.addEventListener('click', onClick);

    return div;
}
function relatedIdsAsString() {
    return relatedProducts.map(function (v) {
        return v.id;
    }).join(' ');
}
