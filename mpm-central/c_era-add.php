<?php

use Form\EraForm;
use Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);


$request = Request::createFromGlobals();
$era = new \TblEra();

$builder = new FormBuilder($twig);

$form = $builder->getForm(EraForm::class, $era);

$form->handleRequest($request);

if ($form->isValid()) {
    $era->save();

    header('Location: c_era-edit.php?id='.$era->getEraId());
    die;
}

echo $twig->render(
    'page_era-manage.html',
    [
        'form' => $form->createView(),
        'add' => true,
		'subPage' => 'cEra',        
    ]);
