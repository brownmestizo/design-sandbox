<?php

use Form\EraForm;
use Form\FormBuilder;
use Form\ProductForm;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);


$request = Request::createFromGlobals();
$era = \TblEraQuery::create()->findOneByEraId($request->get('id'));

$builder = new FormBuilder($twig);

$form = $builder->getForm(EraForm::class, $era);

$form->handleRequest($request);
$result = 0;

if ($form->isValid()) {
    $result = $era->save();
}

echo $twig->render(
    'page_era-manage.html',
    [
        'form' => $form->createView(),
        'add' => false,
        'result' => $result,        
		'subPage' => 'cEra',        
    ]);
