<?php
require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$entities = TblEraQuery::create()->orderByEraDescription('ASC')->find();
$count = TblEraQuery::create()->count();

$entitiesListing = [];

foreach ($entities as $entity) {	

	$countMB = TblProdInfoQuery::create()
	->filterByProdEra($entity->getEraId())
	->where('mb', 1)
	->count();

	$countPA = TblProdInfoQuery::create()
	->filterByProdEra($entity->getEraId())
	->where('pa', 1)
	->count();

	$countM3 = TblProdInfoQuery::create()
	->filterByProdEra($entity->getEraId())
	->where('mb', 1)
	->count();		

	$entitiesListing[] = [
		"description" => $entity->getEraDescription(),
		"id" => $entity->getEraId(),
		"countMB" => $countMB,
		"countPA" => $countPA,
		"countM3" => $countM3,
	];
}

echo $twig->render(
	'page_categories-list.html', 
		array(
			'entities' => $entitiesListing,
			'properName' => 'era',
			'entityName' => 'airplane model era',
			'entityPlural' => 'airplane model eras',
			'count' => $count,
			'add' => true,
			'subPage' => 'cEra',
            'manageUrl' => 'era'
		));
