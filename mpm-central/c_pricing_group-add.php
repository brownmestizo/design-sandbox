<?php
use Form\PricingGroupForm;
use Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$request = Request::createFromGlobals();
$category = new \TblProdPricing();

$builder = new FormBuilder($twig);

$form = $builder->getForm(PricingGroupForm::class, $category);

$form->handleRequest($request);

if ($form->isValid()) {
    $category->save();
    
    header('Location: c_pricing_group-edit.php?id='.$category->getProdPriceId());
    die;    
}

echo $twig->render(
    'page_pricing_group-manage.html',
    [
        'form' => $form->createView(),
        'add' => true,
		'subPage' => 'cPri',        
    ]);