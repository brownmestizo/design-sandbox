<?php
require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$entities = TblProdPricingQuery::create()->orderByProdPriceName('ASC')->find();
$count = TblProdPricingQuery::create()->count();

$entitiesListing = [];

foreach ($entities as $entity) {	
	
	$countMB = TblProdInfoQuery::create()
	->filterByProdPriceId($entity->getProdPriceId())
	->where('mb', 1)
	->count();

	$countPA = TblProdInfoQuery::create()
	->filterByProdPriceId($entity->getProdPriceId())
	->where('pa', 1)
	->count();

	$countM3 = TblProdInfoQuery::create()
	->filterByProdPriceId($entity->getProdPriceId())
	->where('mb', 1)
	->count();		

	$entitiesListing[] = [
		"description" => $entity->getProdPriceName(),
		"id" => $entity->getProdPriceId(),
		"countMB" => $countMB,
		"countPA" => $countPA,
		"countM3" => $countM3,
		"displayValue" => $entity->getProdPricePrice(),
	];
}

echo $twig->render(
	'page_categories-list.html', 
		array(
			'entities' => $entitiesListing,
			'properName' => 'pricing_group',
			'entityName' => 'pricing group',
			'entityPlural' => 'pricing groups',
			'count' => $count,	
			'add' => true,
			'manageUrl' => 'pricing_group',	
			'subPage' => 'cPri',								
		));
?>