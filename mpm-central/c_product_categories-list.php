<?php
require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$entities = TblMenusQuery::create()->orderByMenuName('ASC')->find();
$count = TblMenusQuery::create()->count();

$entitiesListing = [];

foreach ($entities as $entity) {	
	
	$countMB = TblProdInfoQuery::create()
	->filterByProdCategory($entity->getMenuAlias())
	->where('mb', 1)
	->count();

	$countPA = TblProdInfoQuery::create()
	->filterByProdCategory($entity->getMenuAlias())
	->where('pa', 1)
	->count();

	$countM3 = TblProdInfoQuery::create()
	->filterByProdCategory($entity->getMenuAlias())
	->where('mb', 1)
	->count();		

	$entitiesListing[] = [
		"description" => $entity->getMenuName(),
		"id" => $entity->getMenuAlias(),
		"countMB" => $countMB,
		"countPA" => $countPA,
		"countM3" => $countM3,
	];
}

echo $twig->render(
	'page_categories-list.html', 
		array(
			'entities' => $entitiesListing,
			'properName' => 'model_category',
			'entityName' => 'model category',
			'entityPlural' => 'model categories',
			'count' => $count,
			'add' => false,
			'subPage' => 'cMod',
            'manageUrl' => 'product_category'
		));
?>
