<?php

use Form\ProductCategoryForm;
use Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);


$request = Request::createFromGlobals();
$category = new \TblMenus();

$builder = new FormBuilder($twig);

$form = $builder->getForm(ProductCategoryForm::class, $category);

$form->handleRequest($request);

if ($form->isValid()) {
    $category->save();

    header('Location: c_product_category-add.php?add=1&id='.$category->getMenuId());
    die;
}

echo $twig->render(
    'page_product_category-manage.html',
    [
        'form' => $form->createView(),
        'add' => true,
    ]);
