<?php

use Form\ProductCategoryForm;
use Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);


$request = Request::createFromGlobals();
$category = \TblMenusQuery::create()->findOneByMenuAlias($request->get('id'));

if (!$request->get('id') or is_null($category)) {
    header('Location: error.php');
    die();
}

$builder = new FormBuilder($twig);

$form = $builder->getForm(ProductCategoryForm::class, $category);

$form->handleRequest($request);
$result = 0;

if ($form->isValid()) {
    $result = $category->save();
}

echo $twig->render(
    'page_product_category-manage.html',
    [
        'form' => $form->createView(),
        'add' => false,
        'result' => $result,        
		'subPage' => 'cMod',        
    ]);
