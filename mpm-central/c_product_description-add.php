<?php
use Form\ProductDescriptionForm;
use Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$request = Request::createFromGlobals();
$category = new \TblGeneral();

$builder = new FormBuilder($twig);

$form = $builder->getForm(ProductDescriptionForm::class, $category);

$form->handleRequest($request);

if ($form->isValid()) {
    $category->save();

    header('Location: c_product_description-edit.php?id='.$category->getProdGeneral());
    die;
}

echo $twig->render(
    'page_product_description-manage.html',
    [
        'form' => $form->createView(),
        'add' => true,
		'subPage' => 'cPro',							        
    ]);
