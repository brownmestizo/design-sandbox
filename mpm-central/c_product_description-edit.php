<?php
use Form\ProductDescriptionForm;
use Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$request = Request::createFromGlobals();
$category = \TblGeneralQuery::create()->findOneByProdGeneral($request->get('id'));

if (!$request->get('id') or is_null($category)) {
    header('Location: error.php');
    die();
}

$builder = new FormBuilder($twig);

$form = $builder->getForm(ProductDescriptionForm::class, $category);

$form->handleRequest($request);
$result = 0;

if ($form->isValid()) {
    $result = $category->save();
}

echo $twig->render(
    'page_product_description-manage.html',
    [
        'form' => $form->createView(),
        'add' => false,
        'result' => $result,        
		'subPage' => 'cPro',
    ]);