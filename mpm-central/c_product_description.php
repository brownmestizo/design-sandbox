<?php
use Form\ProductDescriptionForm;
use Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$request = Request::createFromGlobals();

if (!$request->get('id')) {
    //if there's no ID parameter, then it's add
    $category = new \TblGeneral();
    $addStatus = true;

} else {
    //otherwise, it's edit
    $category = \TblGeneralQuery::create()->findOneByProdGeneral($request->get('id'));
    $addStatus = false;
}

$builder = new FormBuilder($twig);
$form = $builder->getForm(ProductDescriptionForm::class, $category);

$result = 0;

if ($form->isValid()) {
    $category->save();
}

echo $twig->render(
    'page_product_description-manage.html',
    [
        'form' => $form->createView(),
        'add' => $addStatus,
		'subPage' => 'cPro',   
        'category' => $category,     
        'result' => $result,
    ]);