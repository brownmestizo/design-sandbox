<?php
require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$entities = TblGeneralQuery::create()->orderByProdName('ASC')->find();
$count = TblGeneralQuery::create()->count();

$entitiesListing = [];

foreach ($entities as $entity) {	

	$countMB = TblProdInfoQuery::create()
	->filterByProdGeneral($entity->getProdGeneral())
	->where('mb', 1)
	->count();

	$countPA = TblProdInfoQuery::create()
	->filterByProdGeneral($entity->getProdGeneral())
	->where('pa', 1)
	->count();

	$countM3 = TblProdInfoQuery::create()
	->filterByProdGeneral($entity->getProdGeneral())
	->where('mb', 1)
	->count();		

	$entitiesListing[] = [
		"description" => $entity->getProdName(),
		"id" => $entity->getProdGeneral(),
		"countMB" => $countMB,
		"countPA" => $countPA,
		"countM3" => $countM3,
	];
}

echo $twig->render(
	'page_categories-list.html', 
		array(
			'entities' => $entitiesListing,
			'properName' => 'product_description',
			'entityName' => 'product description',
			'entityPlural' => 'product descriptions',
			'count' => $count,
			'add' => true,
			'manageUrl' => 'product_description',
			'subPage' => 'cPro',							
		));
?>