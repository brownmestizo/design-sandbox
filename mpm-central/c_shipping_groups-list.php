<?php
require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$entities = TblShippingCategoriesQuery::create()->orderByProdShippingName('ASC')->find();
$count = TblShippingCategoriesQuery::create()->count();

$entitiesListing = [];

foreach ($entities as $entity) {	
	
	$countTotal = TblProdInfoQuery::create()
	->filterByProdCategoryShipping($entity->getProdShippingPriceId())	
	->count();	
	
	$countMB = TblProdInfoQuery::create()
	->filterByProdCategoryShipping($entity->getProdShippingPriceId())
	->where('mb', 1)
	->count();

	$countPA = TblProdInfoQuery::create()
	->filterByProdCategoryShipping($entity->getProdShippingPriceId())
	->where('pa', 1)
	->count();

	$countM3 = TblProdInfoQuery::create()
	->filterByProdCategoryShipping($entity->getProdShippingPriceId())
	->where('mb', 1)
	->count();		

	$entitiesListing[] = [
		"description" => $entity->getProdShippingName(),
		"id" => $entity->getProdShippingPriceId(),
		"count" => $countTotal,
		"countMB" => $countMB,
		"countPA" => $countPA,
		"countM3" => $countM3,
	];
}

//Update here
echo $twig->render(
	'page_categories-list.html', 
		array(
			'entities' => $entitiesListing,
			'entityName' => 'shipping category',
			'entityPlural' => 'shipping categories',
			'count' => $count,
			'add' => true,
			'manageUrl' => 'shipping_group',	
			'subPage' => 'cShi',						
		));
?>