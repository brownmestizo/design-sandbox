<?php
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$r = Request::createFromGlobals();
$ids = array_filter(array_map('intval', explode(',', $r->get('ids', ''))));


$type = $r->get('type');

if ($type == 'product') {
	$sql = TblProdInfoQuery::create()->filterByProdId($ids)->delete();
	header('Location: products-list.php');
} elseif ($type == 'product_description') {
	$sql = TblGeneralQuery::create()->filterByProdGeneral($ids)->delete();
	header('Location: c_product_descriptions-list.php');	
} elseif ($type == 'era') {
	$sql = TblEraQuery::create()->filterByEraId($ids)->delete();
	header('Location: c_eras-list.php');	
} elseif ($type == 'pricing_group') {
	$sql = TblProdPricingQuery::create()->filterByProdPriceId($ids)->delete();
	header('Location: c_pricing_groups-list.php');	
}
