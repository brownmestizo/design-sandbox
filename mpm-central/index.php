<?php
require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$countMb = TblProdInfoQuery::create()->filterByMb(1)->count();
$countPa = TblProdInfoQuery::create()->filterByPa(1)->count();	
$countMt = TblProdInfoQuery::create()->filterByM3(1)->count();	


//Models
$countAll = TblProdInfoQuery::create()->count();
$models = TblProdInfoQuery::create()->orderByProdId('DESC')->limit(5)->find();
$modelListing = [];

foreach ($models as $model) {	
	$modelListing[] = [
		"name" => $model->getProdName(),
		"id" => $model->getProdId(),
	];
}


//Eras
$erasRC = TblEraQuery::create()->count();
$eras = TblEraQuery::create()->orderByEraId('ASC')->limit(5)->find();
$erasListing = [];

foreach ($eras as $era) {	
	$count = TblProdInfoQuery::create()
	->filterByProdEra($era->getEraId())
	->count();

	$erasListing[] = [
		"era" => $era->getEraDescription(),
		"qty" => $count,
	];
}


//Pricing categories
$pricingCategoriesRC = TblProdPricingQuery::create()->count();
$pricingCategories = TblProdPricingQuery::create()->orderByProdPriceId('ASC')->limit(5)->find();
$pricingCategoriesListing = [];

foreach ($pricingCategories as $pricingCategory) {	
	$count = TblProdInfoQuery::create()
	->filterByProdPriceId($pricingCategory->getProdPriceId())
	->count();

	$pricingCategoriesListing[] = [
		"pricingCategory" => $pricingCategory->getProdPriceName(),
		"qty" => $count,
	];
}


//Shipping categories
$shippingCategoriesRC = TblProdShippingQuery::create()->count();	
$shippingCategories = TblProdShippingQuery::create()->orderByProdShippingName('ASC')->limit(5)->find();
$shippingCategoriesListing = [];

foreach ($shippingCategories as $shippingCategory) {	
	$count = TblProdInfoQuery::create()
	->filterByProdCategoryShipping($shippingCategory->getProdShippingPriceId())
	->count();

	$shippingCategoriesListing[] = [
		"shippingCategory" => $shippingCategory->getProdShippingName(),
		"qty" => $count,
	];
}


//Product categories
$productCategoriesRC = TblMenusQuery::create()->count();
$productCategories = TblMenusQuery::create()->orderByMenuName('ASC')->limit(5)->find();
$productCategoriesListing = [];

foreach ($productCategories as $productCategory) {	
	$count = TblProdInfoQuery::create()
	->filterByProdCategory($productCategory->getMenuAlias())
	->count();

	$productCategoriesListing[] = [
		"productCategory" => $productCategory->getMenuName(),
		"qty" => $count,
	];
}


//SEO description
$seoDescriptionsRC = TblGeneralQuery::create()->count();
$seoDescriptions = TblGeneralQuery::create()->orderByProdName('ASC')->limit(5)->find();
$seoDescriptionsListing = [];

foreach ($seoDescriptions as $seoDescription) {	
	$count = TblProdInfoQuery::create()
	->filterByProdGeneral($seoDescription->getProdGeneral())
	->count();

	$seoDescriptionsListing[] = [
		"seoDescription" => $seoDescription->getProdName(),
		"qty" => $count,
	];
}




echo $twig->render(
	'page_dashboard.html', 
		array(
			'countMb' => $countMb,
			'countPa' => $countPa,			
			'countMt' => $countMt,	

			'erasRC' => ($erasRC-5),
			'eras' => $erasListing,	

			'pricingCategoriesRC' => ($pricingCategoriesRC-5),					
			'pricingCategories' => $pricingCategoriesListing,

			'shippingCategoriesRC' => ($shippingCategoriesRC-5),			
			'shippingCategories' => $shippingCategoriesListing,
			
			'productCategoriesRC' => ($productCategoriesRC-5),		
			'productCategories' => $productCategoriesListing,

			'seoDescriptionsRC' => ($seoDescriptionsRC-5),		
			'seoDescriptions' => $seoDescriptionsListing,

			'countAll' => ($countAll-5),
			'modelListing' => $modelListing,

		));

?>
