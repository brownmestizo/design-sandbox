<?php
use Form\FormBuilder;
use Form\ProductForm;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

$categories = TblMenusQuery::create()
    ->orderByMenuName('ASC')
    ->find();

$eras = TblEraQuery::create()
    ->orderByEraDescription('ASC')
    ->find();

$pricingCategories = TblProdPricingQuery::create()
    ->orderByProdPriceName('ASC')
    ->find();

$shippingCategories = TblProdShippingQuery::create()
    ->orderByProdShippingName('ASC')
    ->find();

$genericDescriptions = TblGeneralQuery::create()
    ->orderByProdName('ASC')
    ->find();

$request = Request::createFromGlobals();
$product = new \TblProdInfo();
$parentObj = null;

if ($parent = intval($request->get('parent', null))) {
    $parentObj = TblProdInfoQuery::create()->findPk($parent);
    if ($parentObj) {
        $product = copyParent($parentObj);
    }
}

$builder = new FormBuilder($twig);

$form = $builder->getForm(ProductForm::class, $product, [
    'shippingCategories' => $builder->makeChoice($shippingCategories, 'getProdShippingName', 'getProdShippingPriceId'),
    'pricingCategories' => $builder->makeChoice($pricingCategories, 'getProdPriceName', 'getProdPriceId'),
    'genericDescriptions' => $builder->makeChoice($genericDescriptions, 'getProdName', 'getProdGeneral'),
    'eras' => $builder->makeChoice($eras, 'getEraDescription', 'getEraId'),
    'categories' => $builder->makeChoice($categories, 'getMenuName', 'getMenuAlias'),
]);

$form->handleRequest($request);

if ($form->isValid()) {
    if ($form->get('tbl_prod_small_enabled')->getData() != 1) {
        $product->getTblProdSmaller()->delete();
    } else {
        $product->getTblProdSmaller()->setSmEnableId(1);
    }
    $product->setProdKeywordsWriteup(''); // should be filled
    $product->setProdWriteup('');
    $product->setProdRelated2(''); // should be filled
    $product->setProdSaveas(0); // should be filled
    $product->setProdRelated(PkList::fromString($product->getProdRelated())->toString());

    $product->save();

    header('Location: product-edit.php?id='.$product->getProdId());
    die;
}

echo $twig->render(
    'page_product-manage.html',
    [
        'form' => $form->createView(),
        'product' => null,
        'add' => true,
        'parentObj' => $parentObj,
    ]);

/**
 * @param \TblProdInfo $parentObj
 * @return mixed
 * @throws \Propel\Runtime\Exception\PropelException
 */
function copyParent($parentObj)
{
    $product = $parentObj->copy(true);
    if ($product->getTblProdPhotos()) {
        $product->getTblProdPhotos()->setProdId(null);
        $product->getTblProdPhotos()->setTblProdInfo($product);
    }
    if ($product->getTblProdPrices()) {
        $product->getTblProdPrices()->setProdId(null);
        $product->getTblProdPrices()->setTblProdInfo($product);
    }
    if ($product->getTblProdSmaller()) {
        $product->getTblProdSmaller()->setProdId(null);
        $product->getTblProdSmaller()->setTblProdInfo($product);
    }
    return $product;
}
