<?php
use Form\FormBuilder;
use Form\ProductForm;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

//$loader = new Twig_Loader_Filesystem('views/');
//$twig = new Twig_Environment($loader);
$twig = \MB\Templating\Templater::getTwig();
$request = Request::createFromGlobals();
$product = \TblProdInfoQuery::create()->findByProdId($request->get('id'))->getFirst();

$categories = TblMenusQuery::create()
    ->orderByMenuName('ASC')
    ->find();

$eras = TblEraQuery::create()
    ->orderByEraDescription('ASC')
    ->find();

$pricingCategories = TblProdPricingQuery::create()
    ->orderByProdPriceName('ASC')
    ->find();

$shippingCategories = TblProdShippingQuery::create()
    ->orderByProdShippingName('ASC')
    ->find();

$genericDescriptions = TblGeneralQuery::create()
    ->orderByProdName('ASC')
    ->find();

if (!$product->getTblProdPrices()) {
    $product->setTblProdPrices(new \TblProdPrices());
}
if (!$product->getTblProdSmaller()) {
    $product->setTblProdSmaller(new \TblProdSmaller());
}
if (!$product->getTblProdPhotos()) {
    $product->setTblProdPhotos(new \TblProdPhotos());
}

$formBuilder = new FormBuilder($twig);
$form = $formBuilder->getForm(ProductForm::class, $product, [
    'shippingCategories' => $formBuilder->makeChoice($shippingCategories, 'getProdShippingName',
        'getProdShippingPriceId'),
    'pricingCategories' => $formBuilder->makeChoice($pricingCategories, 'getProdPriceName', 'getProdPriceId'),
    'genericDescriptions' => $formBuilder->makeChoice($genericDescriptions, 'getProdName', 'getProdGeneral'),
    'eras' => $formBuilder->makeChoice($eras, 'getEraDescription', 'getEraId'),
    'categories' => $formBuilder->makeChoice($categories, 'getMenuName', 'getMenuAlias'),
]);

$form->handleRequest(Request::createFromGlobals());

if ($form->isValid()) {
    if ($form->get('tbl_prod_small_enabled')->getData() != 1) {
        $product->getTblProdSmaller()->delete();
    } else {
        $product->getTblProdSmaller()->setSmEnableId(1);
    }
    /** @var TblProdInfo $product */
    $product = $form->getData();

    $product->setProdKeywordsWriteup(''); // should be filled
    $product->setProdRelated2(''); // should be filled
    $product->setProdWriteup(''); // should be filled
    $product->setProdSaveas(0); // should be filled
    $pricing = $product->getTblProdPrices();
    $pricing->setProdCustomprice('-');
    $product->setProdRelated(PkList::fromString($product->getProdRelated())->toString());
    
    $product->save();
}

$relatedRows = TblProdInfoQuery::create()
    ->select(['prod_id', 'prod_name'])
    ->findPks($product->getRelatedProductsIds())->toArray();
$relatedProducts = array_map(function ($info) {
    return [
        'id' => $info['prod_id'],
        'name' => $info['prod_name'],
    ];
}, $relatedRows
);

$form = $form->createView();

echo $twig->render(
    'page_product-manage.html',
    compact('form', 'product', 'relatedProducts')
);
