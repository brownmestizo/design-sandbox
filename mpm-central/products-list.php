<?php
use Form\FormBuilder;
use Form\ProductsFilterForm;
use Repository\SpecificationRepository;
use Repository\Specifications\AndSpec;
use Repository\Specifications\Product\CategoriesSpec;
use Repository\Specifications\Product\NameSpec;
use Symfony\Component\HttpFoundation\Request;
use Propel\Runtime\ActiveQuery\Criteria;

require_once '../lib/init.php';

$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);

//Variables
$request = Request::createFromGlobals();
$pageNumber = $request->get('page', 1);

$categories = TblMenusQuery::create()
    ->orderByMenuName('ASC')
    ->find();

$productsQuery = TblProdInfoQuery::create()
    ->orderByProdId('DESC');

$fb = new FormBuilder($twig);
$filter = $fb->getForm(ProductsFilterForm::class, null, [
    'method' => 'GET',
    'categories' => $fb->makeChoice($categories, 'getMenuName', 'getMenuAlias')
]);
$filter->handleRequest($request);
$term = '';

if ($filter->isValid()) {
    $repository = new SpecificationRepository($productsQuery);

    /** @var \Form\SearchDTO $search */
    $search = $filter->getData();
    $term = $search->search;
    $searchTerms = explode(' ', $search->search ?: '');

    $repository->findBySpec(new AndSpec([
        new CategoriesSpec($search->categories),
        new NameSpec($searchTerms),
    ]));
    $productsQuery->clearOrderByColumns();
    switch ($search->sort) {
        case 'new':
            $productsQuery->orderByProdId(Criteria::DESC);
            break;
        case 'old':
            $productsQuery->orderByProdId(Criteria::ASC);
            break;
        case 'az':
            $productsQuery->orderByProdName(Criteria::ASC);
            break;
        case 'za':
            $productsQuery->orderByProdName(Criteria::DESC);
            break;
    }
}

$productsPager = $productsQuery->paginate($pageNumber, $maxResultsPerPage);

$count = $productsPager->getNbResults();

$form = $filter->createView();
$params = $request->query->all();
echo $twig->render('page_products-list.html', compact('productsPager', 'pageNumber', 'count', 'form', 'params', 'term'));
