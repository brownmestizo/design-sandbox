<?php

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PricingGroupForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('prod_price_name', TextType::class, [
            'label' => 'Product description name'
        ])->add('prod_price_description', TextType::class, [
            'label' => 'Description',
        ])->add('prod_price_price', TextType::class, [
            'label' => 'Price'
        ]);

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'POST',
            'data_class' => \TblProdPricing::class
        ]);
    }
}
