<?php

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductCategoryForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('menu_name', TextType::class, [
            'label' => 'Menu name'
        ])->add('menu_title', TextType::class, [
            'label' => 'Menu title'
        ])->add('menu_status', TextareaType::class, [
            'label' => 'Meta keywords'
        ])->add('menu_description', TextareaType::class, [
            'label' => 'Meta description'
        ])->add('menu_writeup_keywords', TextareaType::class, [
            'label' => 'Category keywords'
        ])->add('menu_writeup', TextareaType::class, [
            'label' => 'Category description',
            'attr' => ['class' => 'js-wysy']
        ])->add('menu_status_pa', TextareaType::class, [
            'label' => 'Meta keywords'
        ])->add('menu_description_pa', TextareaType::class, [
            'label' => 'Meta description'
        ])->add('menu_writeup_keywords_pa', TextareaType::class, [
            'label' => 'Category keywords'
        ])->add('menu_writeup_pa', TextareaType::class, [
            'label' => 'Category description',
            'attr' => ['class' => 'js-wysy']
        ])->add('menu_status_m3', TextareaType::class, [
            'label' => 'Meta keywords'
        ])->add('menu_description_m3', TextareaType::class, [
            'label' => 'Meta description'
        ])->add('menu_writeup_keywords_m3', TextareaType::class, [
            'label' => 'Category keywords'
        ])->add('menu_writeup_m3', TextareaType::class, [
            'label' => 'Category description',
            'attr' => ['class' => 'js-wysy']
        ]);

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'POST',
            'data_class' => \TblMenus::class
        ]);
    }
}
