<?php

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductDescriptionForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('prod_name', TextType::class, [
            'label' => 'Name'
        ])->add('prod_description', TextareaType::class, [
            'label' => 'Description',
            'attr' => ['class' => 'js-wysy']
        ])->add('prod_keywords', TextareaType::class, [
            'label' => 'Additional keywords'
        ])->add('prod_description_pa', TextareaType::class, [
            'label' => 'Description',
            'attr' => ['class' => 'js-wysy']
        ])->add('prod_keywords_pa', TextareaType::class, [
            'label' => 'Additional keywords'
        ])->add('prod_description_m3', TextareaType::class, [
            'label' => 'Description',
            'attr' => ['class' => 'js-wysy']
        ])->add('prod_keywords_m3', TextareaType::class, [
            'label' => 'Additional keywords'                        
        ]);

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'POST',
            'data_class' => \TblGeneral::class
        ]);
    }
}
