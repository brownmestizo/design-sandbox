<?php

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductsFilterForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', HiddenType::class)
            ->add('categories', ChoiceType::class, [
                'label' => 'Category',
                'placeholder' => 'Select a category',
                'choices' => $options['categories'],
                'choices_as_values' => true,
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('sort', ChoiceType::class, [
                'label' => 'Sort',
                'choices_as_values' => true,
                'multiple' => false,
                'required' => true,
                'expanded' => true,
                'choices' => [
                    'New first' => 'new',
                    'Old first' => 'old',
                    'A-Z' => 'az',
                    'Z-A' => 'za',
                ],
                'empty_data' => 'new',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'categories'
        ])->setDefaults([
            'data_class' => SearchDTO::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'search';
    }
}
