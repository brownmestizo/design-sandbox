<?php

namespace Form;

class SearchDTO
{
    public $categories = [];
    public $sort = [];
    public $search;
}
