<?php

namespace Form;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use TblProdPhotosQuery;

class UploadedPhotos
{
    /** @var UploadedFile[] */
    public $p;

    /** @var int Id of product to copy */
    public $copy;

    /** @var bool if we want to delete photos */
    public $delete;

    public function getFileDirectory($suffix)
    {
        $path = __DIR__ . '/../../../mpm/uploads' . \strtolower($suffix) . '/';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $realpath = realpath($path);

        return $realpath;
    }

    public function getTblPhotos(\TblProdInfo $info, $suffix = '')
    {
        if (!$photos = $info->getTblProdPhotos()) {
            $photos = new \TblProdPhotos();
            $info->setTblProdPhotos($photos);
        }

        if ($this->delete == 'delete') {
            for ($i = 1; $i < 5; $i++) {
                $photos->{'setProdSolo' . $i . $suffix}('');
            }
        }

        if ($this->copy) {
            $prod = TblProdPhotosQuery::create()->findOneByProdId($this->copy);
            for ($i = 1; $i < 5; $i++) {
                $photos->{'setProdSolo' . $i . $suffix}(
                    $this->copyFile($info->getProdId(), $suffix, $i, $prod)
                );
            }
            $this->copy = null;
        }
        $i = 1;

        foreach ($this->p as $p) {
            if (!$p instanceof UploadedFile) {
                continue;
            }
            $v = $this->uploadFile($info->getProdId(), $p, $i, $suffix);
            if ($v) {
                if ($i == 1) {
                    $photos->{'setProdSolo1' . $suffix}('');
                    $photos->{'setProdSolo2' . $suffix}('');
                    $photos->{'setProdSolo3' . $suffix}('');
                    $photos->{'setProdSolo4' . $suffix}('');
                }
                $photos->{'setProdSolo' . $i++ . $suffix}($v);
            }
            if ($i > 4) {
                return $photos;
            }
        }


        return $photos;
    }

    public function copyFile($id, $suffix, $num, \TblProdPhotos $prod)
    {
        $var = $prod->{'getProdSolo' . $num . $suffix}();
        if (!$var) {
            return '';
        }
        $sourceName = sprintf('%s/%s', $this->getFileDirectory($suffix), $var);
        $ext = pathinfo($sourceName, PATHINFO_EXTENSION);

        $fileName = sprintf('%d%s_%d.%s', $id, $suffix, $num, $ext);
        if (is_file($this->getFileDirectory($suffix) . '/' . $fileName)) {
            @unlink($this->getFileDirectory($suffix) . '/' . $fileName);
        }

        copy($sourceName, $this->getFileDirectory($suffix) . '/' . $fileName);

        return $fileName;
    }

    private function uploadFile($prodId, UploadedFile $file, $num, $suffix)
    {
        $ext = strtolower($file->getClientOriginalExtension());
        if (!in_array($ext, ['jpg', 'png', 'gif', 'jpeg'])) {
            throw new \Exception('Photo is not an image');
        }
        $fileName = sprintf('%d%s_%d.%s', $prodId, $suffix, $num, $ext);
        if (is_file($this->getFileDirectory($suffix) . '/' . $fileName)) {
            @unlink($this->getFileDirectory($suffix) . '/' . $fileName);
        }

        $directory = $this->getFileDirectory($suffix);

        if (!\is_dir($directory)) {
            \mkdir($directory, 0777, true);
        }
        $file->move($directory, $fileName);
        $this->{'p' . $num} = null;

        return $fileName;
    }
}
