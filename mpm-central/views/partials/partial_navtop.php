<div class="row">

    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>        
        
        <div class="navbar-search">
            <label for="main-search">
                <i class="fa fa-search"></i>
            </label>
            <form role="search" class="navbar-form-custom" action="products-list.php">
                <div class="form-group">
                    <input type="text" placeholder="Search products" class="form-control"
                           name="search[search]" value="{{ term|default('') }}" id="top-search">
                </div>
            </form>
            <a href="products.php" class="search-clear"> <i class="fa fa-times"></i></a>
        </div>     


    </nav>

</div>
