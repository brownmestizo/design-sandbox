<?php
use MB\Templating\Templater;
use Propel\Runtime\ActiveQuery\Criteria;
use Repository\SpecificationRepository;
use Repository\Specifications\AndSpec;
use Repository\Specifications\Product\CategoriesSpec;
use Repository\Specifications\Product\WebsiteSpec;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$twig = Templater::getTwig();

$request = Request::createFromGlobals();
$pageNumber = $request->get('page', 1);
$category = $request->get('category', 1);
$sort = $request->get('sort', 'new');
$sorts = [
    "new" => "Newer first",
    "old" => "Older first",
    "az" => "Sort by name: A-Z",
    "za" => "Sort by name: Z-A",
    "price" => "Sort by price: low to high",
    "pricedesc" => "Sort by price: high to low",
];
if (!in_array($sort, array_keys($sorts))) {
    $sort = 'new';
}

$categoryCurrent = TblMenusQuery::create()
    ->findPK($category);

$categories = TblMenusQuery::create()
    ->find();

$repository = new SpecificationRepository(TblProdInfoQuery::create());
/** @var TblProdInfoQuery $prodQuery */
$prodQuery = $repository->findBySpec(new AndSpec([
    new WebsiteSpec($websiteVars['website']),
    new CategoriesSpec([$categoryCurrent->getMenuAlias()]),
]));

switch ($sort) {
    case 'new':
        $prodQuery->orderByProdId(Criteria::DESC);
        break;
    case 'old':
        $prodQuery->orderByProdId(Criteria::ASC);
        break;
    case 'az':
        $prodQuery->orderByProdName(Criteria::ASC);
        break;
    case 'za':
        $prodQuery->orderByProdName(Criteria::DESC);
        break;
    case 'price':
        $prodQuery->useTblProdPricingQuery()->orderByProdPricePrice(Criteria::ASC)->endUse();
        break;
    case 'pricedesc':
        $prodQuery->useTblProdPricingQuery()->orderByProdPricePrice(Criteria::DESC)->endUse();
        break;
}
$productsPager = $prodQuery->paginate($pageNumber, 20);

$params = compact('category', 'sort');


echo $twig->render(
    'page_category.html',
    compact('productsPager', 'categoryCurrent', 'categories', 'pageNumber',
        'websiteVars', 'params', 'sorts', 'sort', 'category')
);
