<?php
use MB\Cart\Cart;
use MB\Templating\Templater;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$request = Request::createFromGlobals();
$id = $request->get('id');
$mt = $request->get('mt');
$twig = Templater::getTwig();
$cs = new \MB\Cart\DbCartStorage();
$cart = $cs->load($id);
$pm = \TblCartQuery::create()->findPk($id)->getPayment();
$p = json_decode($pm);


echo $twig->render(
    $mt == 'cc' ? 'congratulation_cc.html' : 'congratulation.html', compact('websiteVars', 'cart', 'p')
);
