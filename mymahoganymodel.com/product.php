<?php
use Form\FormBuilder;
use MB\Cart\AddToCartUseCase;
use MB\Cart\DbCartStorage;
use MB\Form\ProductChoiceDTO;
use MB\Form\ProductChoiceForm;
use MB\Templating\Templater;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';
require_once '../lib/currency/currencyConverter.php';

$request = Request::createFromGlobals();

if (!$request->get('id')) {
    //throw new \Exception('product not found', 404);
    header('Location: error.php');
    die();
}

$twig = Templater::getTwig();

$productQuery = new TblProdInfoQuery();
$product = $productQuery->findPK($request->get('id'));


if ($websiteVars['website']=='mb' and ($product->getMb() == 0 || $product->getMb()== NULL) ) {
    header('Location: error.php');
    die();    
} 

if ($websiteVars['website']=='pa' and ($product->getMb() == 0 || $product->getPa()== NULL) ) {
    header('Location: error.php');
    die();
}

if ($websiteVars['website']=='m3' and ($product->getMb() == 0 || $product->getM3()== NULL) ) {
    header('Location: error.php');
    die();
}


$smallerVersion = TblProdSmallerQuery::create()->findPK($request->get('id'));


$modelPrices = array();
$modelPricesJS = array();


//Figuring out which product price to show
if ( $product->getProdPriceId() > 0 )  
    $price = $product->getTblProdPricing()->getProdPricePrice();
else 
    $price = $product->getTblProdPrices()->getProdNormalPrice();

$modelPrices = [1 => sprintf('Regular model ($%s)', number_format($price, 2, '.', ','))];
$modelPricesJS = [1 => number_format($price, 2, '.', ',')];


//Smaller models
if (count($smallerVersion) > 0) {
    $modelPrices[2] = sprintf(
        'Smaller model ($%s)', number_format($smallerVersion->getTblProdPricing()->getProdPricePrice(), 2, '.', ',')
    );
    $modelPricesJS[2] = number_format($smallerVersion->getTblProdPricing()->getProdPricePrice(), 2, '.', ',');
}

$pricingQuery = new TblProdPricingQuery();

$relatedProducts = TblProdInfoQuery::create()->findPKs($product->getRelatedProductsIds());

$standsQuery = new TblStandsQuery();
$stands = TblStandsQuery::create()->orderByStandId()->find();

$standId = $request->query->get('stand', null);
$size = $request->query->get('size', null);

$productChoiceDTO = new ProductChoiceDTO();
$productChoiceDTO->product = $size;
$productChoiceDTO->stand = $standId;

$builder = new FormBuilder($twig);
$form = $builder->getForm(ProductChoiceForm::class, $productChoiceDTO, [
    'stands' => $builder->makeChoice($stands, 'getStandNameWithPrice', 'getStandId'),
    'price' => $modelPrices,
    'method' => 'POST',
]);

$form->handleRequest($request);

if ($form->isValid()) {
    $addedItem = $productChoiceDTO;
    $cs = new DbCartStorage();
    foreach ($stands as $s) {
        if ($s->getStandId() == $addedItem->stand) {
            $stand = $s;
            break;
        }
    }
    $addUseCase = new AddToCartUseCase($cs, $product, $stand, $addedItem->quantity, $addedItem->product, $websiteVars);
    $addUseCase->execute();

    $resp = new RedirectResponse('cart.php');
    $resp->send();
    return;
}

$jsData = [
    'products' => $modelPricesJS,
    'stands' => $stands->toKeyValue('StandId', 'StandPrice')
];


echo $twig->render('page_product.html', [
    'jsData' => $jsData,
    'form' => $form->createView(),
    'product' => $product,
    'smaller' => count($smallerVersion),
    'smallerVersion' => $smallerVersion,
    'relatedProducts' => $relatedProducts,
    'stands' => $stands,
    'websiteVars' => $websiteVars,
]);
