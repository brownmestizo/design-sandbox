<?php
use MB\Cart\DbCartStorage;
use MB\Templating\Templater;

require_once '../lib/init.php';

$twig = Templater::getTwig();
$cs = new DbCartStorage();
$cart = $cs->load();

$paymentStatus = true;

$countries = TblShippingCountriesQuery::create()->select(array('cty_id', 'cty_name'))->find()->toArray();


if ($cart->getItemsCount() > 0 )


echo $twig->render(
    'page_shipping.html', 
        compact('cart', 'generated_image_url', 'websiteVars', 'countries', 'paymentStatus'));

else
echo $twig->render('page_empty-cart.html', compact('websiteVars'));
