<footer>
<div class="footer" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <h3> Support </h3>
                <ul>
                    <li class="supportLi">
                        <p> Trademark and Copyright Notice: Paypal, Yahoo Messenger and MSN Messenger and other related entities. All rights reserved. </p>

                        <p>{{websiteVars.websiteName}} is a trade name of {{websiteVars.websiteName}} Wood Models - a company duly registered in the Philippines.</p>                               
                        <h5 class="m-t-sm">
                            <a class="inline" href="contact-us.php">
                                <i class="fa fa-envelope-o"> </i> sales@{{websiteVars.websiteName|lower}}.com
                            </a>
                        </h5>
                    </li>                    
                </ul>

            </div>

            <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                <h3> Information </h3>
                <ul>
                    <li><a href="pantone-color-chart.php"> Pantone color chart </a></li>
                    <li><a href="production-process.php"> Production process </a></li>
                    <li><a href="terms-and-conditions.php"> Terms and Conditions </a></li>
                </ul>
            </div>

            <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                <h3> Stay in touch </h3>
                <ul>
                    <li>
                        <div class="input-append newsLatterBox text-center" id="sib_embed_signup">

                            <div class="forms-builder-wrapper">
                                <input type="hidden" id="sib_embed_signup_lang" value="en">
                                <input type="hidden" id="sib_embed_invalid_email_message" value="That email address is not valid. Please try again.">
                                <input type="hidden" name="primary_type" id="primary_type" value="email">

                                <div id="sib_loading_gif_area" style="position: absolute;z-index: 9999;display: none;">
                                  <img src="https://my.sendinblue.com/public/theme/version4/assets/images/loader_sblue.gif" style="display: block;margin-left: auto;margin-right: auto;position: relative;top: 40%;">
                                </div>

                                <form id="theform" name="theform" action="https://my.sendinblue.com/users/subscribeembed/js_id/2pssi/id/{{websiteVars.sendInBlueSubID}}" onsubmit="return false;">
                                    
                                    <input type="hidden" name="js_id" id="js_id" value="2pssi">
                                    <input type="hidden" name="listid" id="listid" value="{{websiteVars.sendInBlueListID}}">
                                    <input type="hidden" name="from_url" id="from_url" value="yes">
                                    <input type="hidden" name="hdn_email_txt" id="hdn_email_txt" value="">

                                    <div class="sib-container rounded">                                            
                                        <input type="hidden" name="req_hid" id="req_hid" value="">
                                        <div class="view-messages" style="margin:5px 0;"> </div>
                                        <div class="primary-group email-group forms-builder-group ui-sortable" style="">
                                            <div class="row mandatory-email" style="padding: 5px 15px 0px 15px">
                                                <input type="text" name="email" id="email" value="" placeholder="Enter your email address" class="full text-center">
                                            </div>
                                        </div>

                                        <button class="button editable btn bg-gray" type="submit" data-editfield="subscribe">
                                            Subscribe
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div>


                        <script type="text/javascript">
                            var sib_prefix = 'sib';
                            var sib_dateformat = 'mm-dd-yyyy';
                        </script>
                        <script type='text/javascript' src='https://my.sendinblue.com/public/theme/version4/assets/js/src/subscribe-validate.js?v=1.0'></script>
                        <script type='text/javascript'>
                            jQuery = jQuery||undefined;
                            if (jQuery) jQuery.noConflict(true);
                        </script>

                    </li>
                </ul>
                <ul class="social">
                    <li><a href="http://facebook.com"> <i class="fa fa-facebook"> &nbsp; </i></a></li>
                    <li><a href="http://twitter.com"> <i class="fa fa-twitter"> &nbsp; </i></a></li>
                    <li><a href="http://youtube.com"> <i class="fa fa-youtube"> &nbsp; </i></a></li>
                </ul>
            </div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!--/.footer-->

<div class="footer-bottom" style="padding: 30px 0 55px;">
    <div class="container">
        <p class="pull-left" style="display: block; clear: both;"> 
            &copy; {{websiteVars.websiteName}} 2008 - {{ 'now'|date('Y') }}. All right reserved.
        </p>

        <div class="pull-left paymentMethodImg" style="display: block; clear: both;">
            <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppppcmcvdam.png" alt="Pay with PayPal, PayPal Credit or any major credit card" />  
        </div>
    </div>
</div>
<!--/.footer-bottom-->
</footer>
