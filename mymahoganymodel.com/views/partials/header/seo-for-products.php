<meta name="keywords" content="{% block keywords %}{% endblock %}">
<meta name="description" content="{% block description %}{% endblock %}">    
<meta name="author" content="{{websiteVars.websiteName}}">
<meta name="ROBOTS" content="ALL">   