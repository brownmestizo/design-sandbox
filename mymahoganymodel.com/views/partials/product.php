                    <div class="product">
                        
                        {% if product.getProdSaveAs() == 1 %}
                        <span class="add-fav tooltipHere" data-toggle="tooltip" data-original-title="Add to Wishlist" data-placement="left">
                            <i class="fa fa-star"></i>
                        </span>
                        {% endif %}                         
                        
                        <div class="image m-b-sm">                     
                            <a href="product.php?id={{ product.getProdId() }}">      
                                <img src="{{ websiteVars.image_url }}{{ product.displayMainImage( websiteVars.website ) }}" alt="img" class="img-responsive" onerror="this.src = '{{ websiteVars.defaultImg }}';">
                            </a>
                        </div>

                        <div class="description">
                            <div class="fluid-ellipsis-container">
                                <div class="fluid-ellipsis-content-wrapper">
                                    <a href="product.php?id={{ product.getProdId() }}">
                                    {{ product.getProdName() }}
                                    </a>
                                </div>
                            </div>                        

                        </div>

                        <!--
                        <div class="price">
                            <small>Starting from</small>
                            <span>${{ product.getTblProdPricing().getProdPricePrice() }}</span> 
                        </div> 
                        -->
                        
                    </div>