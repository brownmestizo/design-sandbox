<?php
use MB\Cart\ChangeQuantityUseCase;
use MB\Cart\DbCartStorage;
use MB\Cart\RemoveFromCartUseCase;
use MB\Templating\Templater;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

require_once '../lib/init.php';

$request = Request::createFromGlobals();
$twig = Templater::getTwig();

$cs = new DbCartStorage();
switch ($request->get('action')) {
    case 'delete':
        $deleteUC = new RemoveFromCartUseCase($cs, $request->get('id'));
        $deleteUC->execute();
        (new RedirectResponse('cart.php'))->send();
        return;
    case 'change':
        $changeUc = new ChangeQuantityUseCase($cs, $request->request->get('id'), $request->request->get('q'));
        $changeUc->execute();
        die('');
}
$cart = $cs->load();

$shippingStatus = true;


if ($cart->getItemsCount() > 0 )
echo $twig->render('page_cart.html', compact('cart', 'generated_image_url', 'websiteVars', 'shippingStatus'));
else
echo $twig->render('page_empty-cart.html', compact('websiteVars'));
