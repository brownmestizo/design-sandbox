<?php
require_once '../lib/init.php';

use MB\Templating\Templater;

$twig = Templater::getTwig();

$latestProductID = TblProdInfoQuery::create()
	->orderByProdId('desc')
	->limit(1)
	->findOne();

$categories = TblMenusQuery::create()
	->orderByMenuId('desc')
	->find();

$newProducts = TblProdInfoQuery::create()
	->useTblProdPhotosQuery()
		->filterByProdId(array('max'=>$latestProductID->getProdId()))
	->endUse()
	->orderByProdId('desc')
	->forThisWebsiteOnly($websiteVars['website'])
	->limit(8);

$featuredProducts = TblProdInfoQuery::create()
	->orderByProdId('asc')
	->filterByProdFront('yes')
	->forThisWebsiteOnly($websiteVars['website'])
	->limit(4)
	->find();	

	
echo $twig->render(
	'page_index.html', 
		array(
			'newProducts' => $newProducts, 
			'featuredProducts' => $featuredProducts, 
			'websiteVars' => $websiteVars,
			'categories' => $categories
		));

?>