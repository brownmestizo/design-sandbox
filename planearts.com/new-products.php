<?php
use MB\Templating\Templater;

require_once '../lib/init.php';

$twig = Templater::getTwig();

// Variables
$pageNumber = isset($_GET['page']) ? $_GET['page'] : 1;

$categories = TblMenusQuery::create()
	->find();	

$latestProductID = TblProdInfoQuery::create()
    ->orderByProdId('desc')
    ->limit(1)
    ->findOne();

$newProducts = TblProdInfoQuery::create()
    ->useTblProdPhotosQuery()
        ->filterByProdId(array('max'=>$latestProductID->getProdId()))
    ->endUse()
    ->where($websiteVars['website'] = 1)   
    ->orderByProdId('desc')
    ->limit(20);    


echo $twig->render(
    'page_newproducts.html', 
        array(
            'newProducts' => $newProducts,
            'websiteVars' => $websiteVars,
            ));

?>