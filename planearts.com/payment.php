<?php
use MB\Cart\CartItem;
use MB\Cart\DbCartStorage;
use MB\Shipping\Shipping;
use MB\Templating\Templater;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

require_once '../lib/init.php';

$twig = Templater::getTwig();
$cs = new DbCartStorage();
$cart = $cs->load();

$request = Request::createFromGlobals();
$shipmentType = $request->get('type', null);
$shipmentCty = $request->get('cty', null);
if (!in_array($shipmentType, ['priority', 'economy', 'air'])) {
    header('Location: shipping.php');
    exit;
}

try {
    $shipping = new Shipping($shipmentCty);
} catch (\Exception $e) {
    header('Location: shipping.php');
    exit;
}
$cart->setPaymentDetails(['type' => $shipmentType, 'cty' => $shipmentCty]);
$cs->save($cart);

$price = $shipping->getPrice($shipmentType, $cart);
$products = $shipping->getProductPrices($cart);
$finalStep = true;

$productDetails = array_map(function (CartItem $cartItem) {
    return [
        'price' => $cartItem->price(),
        'name' => $cartItem->getProduct()->getName() . '(' . $cartItem->getStand()->getName() . ')',
        'quantity' => $cartItem->getQuantity(),
        "currency" => "USD"
    ];
}, $cart->getItems());
$id = (new Session())->getId();


echo $twig->render(
    'page_payment.html',
    compact('cart', 'id', 'generated_image_url', 'websiteVars', 'environment', 'price', 'products', 'shipmentType', 'shipmentCty', 'finalStep', 'productDetails')
);
