<div id="homepage">

    <section id="main-product">
        <div class="container" style="opacity: 1;">
            <h1>Planearts</h1>
            <p class="description">
            Handmade museum quality wood model "off the shelf" airplanes <br />Made of solid kiln dried, renewable Philippine mahogany
            </p>
            <a href="category.php?category=1" class="btn">Browse all our models</a>
        </div>  
    </section>

    <div class="container m-t-lg m-b-lg">
        <div class="row">
            <div class="col-lg-4">
                <div class="blog-card card1">
                    <div class="title-content">
                        <h3>JET PLANES</h3>
                        <div class="intro">British owned, Filipino made <br />From $199</div>
                    </div>

                    <div class="gradient-overlay"></div>
                    <div class="color-overlay"></div>
                </div>     
            </div>

            <div class="col-lg-4">
                <div class="blog-card card2">
                    <div class="title-content">
                        <h3>PROPELLER PLANES</h3>
                        <div class="intro">Hand carved and hand painted <br />From $199</div>
                    </div>

                    <div class="gradient-overlay"></div>
                    <div class="color-overlay"></div>
                </div>     
            </div>
            
            <div class="col-lg-4">
                <div class="blog-card card3">
                    <div class="title-content">
                        <h3>HELICOPTERS</h3>
                        <div class="intro">Finest musem quality model airplanes <br />From $459</div>
                    </div>

                    <div class="gradient-overlay"></div>
                    <div class="color-overlay"></div>
                </div>     
            </div>

            <div class="col-lg-4">
                <div class="blog-card card4">
                    <div class="title-content">
                        <h3>EARLY AVIATION</h3>
                        <div class="intro">Built for creativity on an epic scale <br />From $599</div>
                    </div>

                    <div class="gradient-overlay"></div>
                    <div class="color-overlay"></div>
                </div>     
            </div>
            
            
            <div class="col-lg-4">
                <div class="blog-card card5">
                    <div class="title-content">
                        <h3>AIRSHIPS</h3>
                        <div class="intro">Will last for generations to come <br />From $499</div>
                    </div>

                    <div class="gradient-overlay"></div>
                    <div class="color-overlay"></div>
                </div>     
            </div>
            
           
        </div>
    </div>    








</div>

<!--
<div class="parallax-section parallax-image-1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="homepage-banner parallax-content clearfix">

                </div>
            </div>
        </div>
    </div>
</div>
-->

<div class="container m-t-lg">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        

            <p class="lead" style="margin-top: 40px;">{{websiteVars.websiteName}} is a one stop factory source for handmade museum quality wood model airplanes and wooden helicopter models. Stock, bespoke and custom made personalised scale handcrafted and hand painted wooden model aircraft, helicopters, spacecraft, rockets, airships and blimps, tail fins, boats, submarines  and ships, military wall plaques and seals all made out of solid kiln dried, renewable Philippine mahogany (commonly known as Lauan or Meranti).</p>

            <p>As a British owned company with manufacturing facilities in the Philippines, {{websiteVars.websiteName}}.com® offers custom made and standard "off the shelf" authentic quality wooden model aircraft in Private & Civilian, Military / Warplane Jet, Military Propeller Warplanes, Glider, Airship, Submarine, Ship, Boat, Science Fiction, Space, NASA and Experimental  categories - even  aviation "nose art" panels.  Unlike our competitors {{websiteVars.websiteName}}.com® run their business direct from the factory source and not through third party companies or regional trading offices.</p>  

            <p>The quality of your model replica is assured from day one of manufacture.  Our reputable model airplanes are ideally suited as collectibles, executive gifts, premium and promotional incentives, art gallery sculptures, and many other purposes.  Each model is hand carved and completely handpainted to match the aircrafts livery, paint scheme and registration number.  Any logo can be carved or painted on the stand and a brass plaque with your personal inscription adds that extra finishing touch to your model. Please visit our <a href="testimonials.php">Testimonial Page</a> to see the kind words that our customers have had to say about our models.</p>

            <p>Manufacturers of the finest museum quality hand made model airplanes (civilian and warplanes ), model helicopters, airships, blimps, dirigibles, tail shields, wall plaques and seals, boats, ships and submarines. Custom made and stock quality wooden collectible models direct from the maker’s factory.</p>

            <p>With production facilities near the former Clark Air Base in the Philippines, {{websiteVars.websiteName}} is a pilot owned and operated company selling hand painted wood model airplanes direct from our factory and not through marketing or distribution companies. The exactness and detail of your model is guaranteed from start to finish.  Ideal for birthday or Christmas gifts, presentations, collector’s items, desktops, showcases, museums, flying schools, reception areas, offices and many aviation or other related locations.</p>

            <p>All models shown on our website have been hand carved and hand painted by our own highly talented master craftsmen. The model you receive will be identical in looks and quality to those you see in the hundreds of photographs on our website (these photographs have not been digitally enhanced and reflect the true features of the model).</p> 

            <p>Unfortunately even a photo cannot show what you will see or feel in real life.  There is no plastic, resin, die cast or artificially made model that will come close to unique warm feel and touch of a truly hand made wood model .  These models will last for generations to come and will become family heirlooms. Please visit our <a href="testimonials.php">Testimonial Page</a> to see the kind words that have been said about our models.</p>

            <p>If you do not find the model you require in our extensive database we can make a customised model of your own or favourite airplane, warplane,  helicopter or airship by sending your specifications and digital images through email to <a href="mailto:info@planearts.com">info@planearts.com</a>.  Any plane type painted in any livery or paint scheme and made specifically to your specifications including a custom base with your choice of logo and / or a brass plaque with your personal wording of up to 4 lines. A full quotation will be given within 48 hours and digital photos emailed to you of the final model before despatch.</p>

            <p>Most of our models are made to order and take about 8-10 weeks* to manufacture (even customized models) which is far quicker than most other companies who charge more and take longer!  You will also notice that we offer standard models at prices way below other companies advertising on the web because we ship directly from our factory and not through other marketing or distribution companies.  Models are shipped through FedEx International Economy courier service and delivered worldwide within 5 business days (FedEx Priority also available 1-3 business days) direct to your home whether it be across the Pacific or the Atlantic. Each model includes a stand of your choice and export packing in 300 Pound Strength Cardboard boxes with closely cut foam inserts to ensure the safest possible "ride" for your airplane model. *from date of order confirmation.</p>

            <p>Working from your photographs and our library of aircraft blueprints, our master craftsmen will meticulously recreate your airplane into an amazingly detailed desktop replica. Each model is hand carved from solid mahogany and completely hand painted to match your aircraft paint scheme and registration number. No detail will be spared!</p>

            <p>For more information, please visit our <a href="madetoorder.php">Made to order model</a> section</p>

            <p>{{websiteVars.websiteName}} can make almost any model to the highest of specifications. If we have not made a particular model before then we will get scale plans and use our own digital pictures of the aircraft or get them from stock sources. Color schemes both actual and to your design can be applied, and you can specify the scale size of your model. As it is sometimes difficult for us to see from digital images the exact color of certain parts of the plane it would help if you could let us have the closest Pantone color for each part of section of the body. This can easily done by checking the <a href="pantone-color-chart.php">Pantone Color Chart</a>.</p>

            <p>For further information, <a href="contact-us.php">contact us</a></p>

        </div>
    </div>
</div>
        