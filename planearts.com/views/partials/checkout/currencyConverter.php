

<!-- Button trigger modal -->
<button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#myModal">
  Preview in a different currency
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header" style="margin-bottom: 0px; padding: 10px 10px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Currency preview</h4>
      </div>

      <div class="modal-body">

        <div class="row">
            <div class="col-xs-12">
                <label>Select a currency</label>
                <div class="bfh-selectbox bfh-currencies js-currencies" data-blank="false" data-currency="USD"
                    data-available="{{ currencies() | join(',') }}" data-flags="true"
                    style="width: 100%;">
                </div>

                <p><i class="fa fa-info-circle"></i> Your chosen currency option is only an approximate amount based upon Yahoo! Exchange rates. Final prices upon checkout will still be reflected in USD.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-8">
                Regular Price
            </div>
            <div class="col-xs-4 text-right js-product-modal-regular-price"></div>
        </div>
        <div class="row">
            <div class="col-xs-8 js-product-modal-stand-name"></div>
            <div class="col-xs-4 text-right js-product-modal-stand-price"></div>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <strong>Cost for this product</strong>
            </div>
            <div class="col-xs-4 text-right">
                <strong class="js-product-modal-cost"></strong>
            </div>            
        </div>
          <div class="row">
              <div class="col-xs-8">Quantity</div>
              <div class="col-xs-4 text-right js-product-modal-quantity"></div>
          </div>
          <div class="row">
              <div class="col-xs-8">
                  <strong>Total cost for this product</strong>
              </div>
              <div class="col-xs-4 text-right">
                  <strong class="js-product-modal-total-cost"></strong>
              </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
