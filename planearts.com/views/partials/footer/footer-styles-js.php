
<!-- Le javascript
================================================== -->

<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>

<!-- include jqueryCycle plugin -->
<!--
<script src="../assets/js/jquery.cycle2.min.js"></script>
-->

<!-- include easing plugin -->
<script src="../assets/js/jquery.easing.1.3.js"></script>

<!-- include  parallax plugin -->
<script type="text/javascript" src="../assets/js/jquery.parallax-1.1.js"></script>

<!-- optionally include helper plugins -->
<script type="text/javascript" src="../assets/js/helper-plugins/jquery.mousewheel.min.js"></script>

<!-- include mCustomScrollbar plugin //Custom Scrollbar  -->
<script type="text/javascript" src="../assets/js/jquery.mCustomScrollbar.js"></script>

<!-- include checkRadio plugin //Custom check & Radio  -->
<script type="text/javascript" src="../assets/js/ion-checkRadio/ion.checkRadio.min.js"></script>

<!-- include grid.js // for equal Div height  -->
<script src="../assets/js/grids.js"></script>

<!-- include carousel slider plugin  -->
<script src="../assets/plugins/owlcarousel/owl.carousel.min.js"></script>

<!-- include Bootstrap form helpers -->
<script src="../assets/bootstrap-formhelper/dist/js/bootstrap-formhelpers.min.js"></script>

<!-- include touchspin.js // touch friendly input spinner component   -->
<script src="../assets/js/bootstrap.touchspin.js"></script>

<!-- include custom script for only homepage  -->
<script src="../assets/js/home.js"></script>

<!-- include custom script for site  -->
<script src="../assets/js/script.js"></script>
<script src="../assets/js/home.js"></script>
<script src="../assets/js/custom.js"></script>